/**
 * @file
 * Contains the definition of the behavior entityVisibilityPreviewPreviewMessage.
 */

((Drupal) => {
  /**
   * Attaches the JS behavior to display preview message.
   */
  Drupal.behaviors.entityVisibilityPreviewPreviewMessage = {
    /**
     * As multiple behaviors can be triggered before the Ajax call returns, we
     * need to store the fact that it is already triggered otherwise the message
     * will be displayed multiple times.
     *
     * @type {Boolean}
     */
    already_called: false,

    attach() {
      const messages = new Drupal.Message();

      // Check that the message does not already exist.
      const message = messages.select('entity_visibility_preview');
      if (
        !Drupal.behaviors.entityVisibilityPreviewPreviewMessage
          .already_called &&
        message === null
      ) {
        Drupal.behaviors.entityVisibilityPreviewPreviewMessage.already_called = true;
        // Get the info using AJAX.
        const xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = () => {
          if (
            xhttp.readyState === 4 &&
            xhttp.status === 200 &&
            xhttp.responseText.length !== 0
          ) {
            messages.add(xhttp.responseText, {
              id: 'entity_visibility_preview',
            });
          }
        };
        xhttp.open(
          'GET',
          '/ajax/entity_visibility_preview/preview_message',
          true,
        );
        xhttp.send();
      }
    },
  };
})(Drupal);
