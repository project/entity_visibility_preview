[//]: <> (phpcs:ignoreFile)

Development Notes
-----------------

To regenerate the configuration used in automated tests.

```shell script
drush config:devel-export evp_simple_grants_search_api_db_test
# Depending of the development environment used and depending of the place of your cloned repository.
rsync -avzP --delete /project/app/modules/contrib/entity_visibility_preview/modules/entity_visibility_preview_simple_grants/tests/modules/evp_simple_grants_search_api_db_test/config /project/contrib/entity_visibility_preview/modules/entity_visibility_preview_simple_grants/tests/modules/evp_simple_grants_search_api_db_test/
chmod 644 -R /project/contrib/entity_visibility_preview/modules/entity_visibility_preview_simple_grants/tests/modules/evp_simple_grants_search_api_db_test/config/install/*
```
