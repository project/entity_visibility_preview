<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview_configurable_plugin_test\Plugin\EntityVisibilityPreviewCondition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginBase;

/**
 * Allows to test a configurable plugin.
 *
 * @EntityVisibilityPreviewCondition(
 *     id = "configurable_plugin_test",
 *     label = @Translation("Configurable plugin test"),
 *     dependencies = {"system"}
 * )
 */
class ConfigurablePlugin extends EntityVisibilityPreviewConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $values): array {
    return [
      'test' => [
        '#type' => 'textfield',
        '#title' => $this->t('Test'),
        '#description' => $this->t('This value is not used. Only to set a value in the field.'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(array $values): array {
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(): array {
    $form = [];

    $form['allow_access'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow access'),
      '#default_value' => $this->configuration['allow_access'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function accessCheck(array $values, FieldDefinitionInterface $field_definition): AccessResultInterface {
    if (isset($this->configuration['allow_access']) && $this->configuration['allow_access']) {
      $access_result = AccessResult::neutral();
    }
    else {
      $access_result = AccessResult::forbidden();
    }

    return $access_result;
  }

}
