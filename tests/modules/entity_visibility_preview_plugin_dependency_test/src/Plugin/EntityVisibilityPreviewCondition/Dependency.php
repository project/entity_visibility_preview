<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview_plugin_dependency_test\Plugin\EntityVisibilityPreviewCondition;

use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginBase;

/**
 * Allows to test the dependencies on plugin.
 *
 * @EntityVisibilityPreviewCondition(
 *     id = "dependency_test",
 *     label = @Translation("Dependency test"),
 *     dependencies = {"false"}
 * )
 */
class Dependency extends EntityVisibilityPreviewConditionPluginBase {

}
