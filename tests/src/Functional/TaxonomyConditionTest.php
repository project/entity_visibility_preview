<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\user\UserInterface;

/**
 * Tests for the taxonomy condition plugin.
 *
 * @group entity_visibility_preview
 */
class TaxonomyConditionTest extends EntityVisibilityPreviewFunctionalTestBase {

  use EntityReferenceFieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'taxonomy_condition' => 'taxonomy_condition',
    ],
    'condition_plugins_settings' => [
      'taxonomy_condition' => [
        'bundles' => [
          'taxonomy_condition_test' => 'taxonomy_condition_test',
        ],
        'user_fields' => [
          'test_taxonomy_condition' => 'test_taxonomy_condition',
        ],
      ],
    ],
  ];

  /**
   * The vocabulary ID used for the tests.
   *
   * @var string
   */
  protected $vocabularyId = 'taxonomy_condition_test';

  /**
   * The state key of the taxonomy condition plugin used for the tests.
   *
   * @var string
   */
  protected $stateKey = 'entity_visibility_preview:anonymous:taxonomy_condition:node:article:field_entity_visibility_preview';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->state = $this->container->get('state');

    $taxonomy = Vocabulary::create([
      'name' => $this->randomString(),
      'vid' => $this->vocabularyId,
    ]);
    $taxonomy->save();

    $field_name = 'test_taxonomy_condition';
    $selection_handler_settings = [
      'target_bundles' => [
        $this->vocabularyId => $this->vocabularyId,
      ],
    ];
    $this->createEntityReferenceField('user', 'user', $field_name, $this->randomString(), 'taxonomy_term', 'default', $selection_handler_settings, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
  }

  /**
   * Test the cases of the taxonomy plugin.
   */
  public function testTaxonomyPlugin(): void {
    $node = $this->drupalCreateNode([
      'type' => 'article',
      'status' => NodeInterface::PUBLISHED,
    ]);

    $taxonomy_term_1 = Term::create([
      'name' => 'Taxonomy 1 term',
      'vid' => $this->vocabularyId,
    ]);
    $taxonomy_term_1->save();

    $taxonomy_term_2 = Term::create([
      'name' => 'Taxonomy 2 term',
      'vid' => $this->vocabularyId,
    ]);
    $taxonomy_term_2->save();

    $term_1_uuid = [
      $taxonomy_term_1->uuid(),
    ];
    $term_1_id = [
      $taxonomy_term_1->id(),
    ];
    $term_2_uuid = [
      $taxonomy_term_2->uuid(),
    ];
    $term_2_id = [
      $taxonomy_term_2->id(),
    ];
    $term_1_2_uuid = [
      $taxonomy_term_1->uuid(),
      $taxonomy_term_2->uuid(),
    ];
    $term_1_2_id = [
      $taxonomy_term_1->id(),
      $taxonomy_term_2->id(),
    ];

    // User with no taxonomy terms.
    $user_0 = $this->drupalCreateUser($this->userPermissions);
    if (!($user_0 instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }

    // User with one taxonomy terms.
    $user_1 = $this->drupalCreateUser(
      $this->userPermissions,
      $this->randomString(),
      FALSE,
      [
        'test_taxonomy_condition' => [
          [
            'target_id' => $taxonomy_term_1->id(),
          ],
        ],
      ]
    );
    if (!($user_1 instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }

    // User with one taxonomy terms.
    $user_2 = $this->drupalCreateUser(
      $this->userPermissions,
      $this->randomString(),
      FALSE,
      [
        'test_taxonomy_condition' => [
          [
            'target_id' => $taxonomy_term_2->id(),
          ],
        ],
      ]
    );
    if (!($user_2 instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }

    // User with two taxonomy terms.
    $user_1_2 = $this->drupalCreateUser(
      $this->userPermissions,
      $this->randomString(),
      FALSE,
      [
        'test_taxonomy_condition' => [
          [
            'target_id' => $taxonomy_term_1->id(),
          ],
          [
            'target_id' => $taxonomy_term_2->id(),
          ],
        ],
      ]
    );
    if (!($user_1_2 instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }

    // Anonymous test without taxonomy terms.
    $this->taxonomyAnonymousTest($node, [], [], 200);
    $this->taxonomyAnonymousTest($node, $term_1_uuid, [], 403);
    $this->taxonomyAnonymousTest($node, $term_2_uuid, [], 403);
    $this->taxonomyAnonymousTest($node, $term_1_2_uuid, [], 403);

    // Anonymous test with taxonomy term 1.
    $this->taxonomyAnonymousTest($node, [], $term_1_uuid, 200);
    $this->taxonomyAnonymousTest($node, $term_1_uuid, $term_1_id, 200);
    $this->taxonomyAnonymousTest($node, $term_2_uuid, $term_1_id, 403);
    $this->taxonomyAnonymousTest($node, $term_1_2_uuid, $term_1_id, 200);

    // Anonymous test with taxonomy term 2.
    $this->taxonomyAnonymousTest($node, [], $term_2_id, 200);
    $this->taxonomyAnonymousTest($node, $term_1_uuid, $term_2_id, 403);
    $this->taxonomyAnonymousTest($node, $term_2_uuid, $term_2_id, 200);
    $this->taxonomyAnonymousTest($node, $term_1_2_uuid, $term_2_id, 200);

    // Anonymous test with both taxonomy terms.
    $this->taxonomyAnonymousTest($node, [], $term_1_2_id, 200);
    $this->taxonomyAnonymousTest($node, $term_1_uuid, $term_1_2_id, 200);
    $this->taxonomyAnonymousTest($node, $term_2_uuid, $term_1_2_id, 200);
    $this->taxonomyAnonymousTest($node, $term_1_2_uuid, $term_1_2_id, 200);

    // User without taxonomy terms.
    $this->taxonomyUserTest($node, $user_0, [], 200);
    $this->taxonomyUserTest($node, $user_0, $term_1_uuid, 403);
    $this->taxonomyUserTest($node, $user_0, $term_2_uuid, 403);
    $this->taxonomyUserTest($node, $user_0, $term_1_2_uuid, 403);

    // User with taxonomy term 1.
    $this->taxonomyUserTest($node, $user_1, [], 200);
    $this->taxonomyUserTest($node, $user_1, $term_1_uuid, 200);
    $this->taxonomyUserTest($node, $user_1, $term_2_uuid, 403);
    $this->taxonomyUserTest($node, $user_1, $term_1_2_uuid, 200);

    // User with taxonomy term 2.
    $this->taxonomyUserTest($node, $user_2, [], 200);
    $this->taxonomyUserTest($node, $user_2, $term_1_uuid, 403);
    $this->taxonomyUserTest($node, $user_2, $term_2_uuid, 200);
    $this->taxonomyUserTest($node, $user_2, $term_1_2_uuid, 200);

    // User with both taxonomy terms.
    $this->taxonomyUserTest($node, $user_1_2, [], 200);
    $this->taxonomyUserTest($node, $user_1_2, $term_1_uuid, 200);
    $this->taxonomyUserTest($node, $user_1_2, $term_2_uuid, 200);
    $this->taxonomyUserTest($node, $user_1_2, $term_1_2_uuid, 200);
  }

  /**
   * Helper function.
   *
   * Check status code depending on taxonomy conditions.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node used for tests.
   * @param \Drupal\user\UserInterface $user
   *   The user to log for the test.
   * @param array $taxonomy_term_uuids_to_set
   *   The taxonomy UUIDS to set on the node.
   * @param int $expected_status_code
   *   The expected status code.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function taxonomyUserTest(NodeInterface $node, UserInterface $user, array $taxonomy_term_uuids_to_set, int $expected_status_code = 200): void {
    if (!empty($taxonomy_term_uuids_to_set)) {
      $node->set($this->fieldName, \serialize([
        'taxonomy_condition' => [
          'taxonomy_term_uuids' => $taxonomy_term_uuids_to_set,
        ],
      ]));
    }
    else {
      $node->set($this->fieldName, \serialize([]));
    }
    $node->save();

    $this->drupalLogin($user);
    $this->drupalGet($node->toUrl());
    $this->assertSession()->statusCodeEquals($expected_status_code);
  }

  /**
   * Helper function.
   *
   * Check status code depending on taxonomy conditions.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node used for tests.
   * @param array $taxonomy_term_ids_to_set
   *   The UUIDS to set on the node.
   * @param array $state_value
   *   The TIDs to set in the state for the anonymous TIDs.
   * @param int $expected_status_code
   *   The expected status code.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function taxonomyAnonymousTest(NodeInterface $node, array $taxonomy_term_ids_to_set, array $state_value, int $expected_status_code = 200): void {
    // Set the value for the anonymous.
    $this->state->set($this->stateKey, $state_value);

    if (!empty($taxonomy_term_ids_to_set)) {
      $node->set($this->fieldName, \serialize([
        'taxonomy_condition' => [
          'taxonomy_term_uuids' => $taxonomy_term_ids_to_set,
        ],
      ]));
    }
    else {
      $node->set($this->fieldName, \serialize([]));
    }
    $node->save();

    $this->drupalGet($node->toUrl());
    $this->assertSession()->statusCodeEquals($expected_status_code);
  }

}
