<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;

/**
 * Tests fields formatter.
 *
 * @group entity_visibility_preview
 */
class EntityVisibilityPreviewFieldTest extends EntityVisibilityPreviewFunctionalTestBase {

  use EntityReferenceFieldCreationTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_visibility_preview',
    'field',
    'field_ui',
    'node',
    'taxonomy',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $userPermissions = [
    'access content',
    'entity_visibility_preview_bypass_access',
  ];

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'date_range_condition' => 'date_range_condition',
      'taxonomy_condition' => 'taxonomy_condition',
    ],
    'condition_plugins_settings' => [
      'taxonomy_condition' => [
        'bundles' => [
          'taxonomy_condition_test' => 'taxonomy_condition_test',
        ],
        'user_fields' => [
          'test_taxonomy_condition' => 'test_taxonomy_condition',
        ],
      ],
    ],
  ];

  /**
   * The vocabulary ID used for the tests.
   *
   * @var string
   */
  protected $vocabularyId = 'taxonomy_condition_test';

  /**
   * The tested node.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * The display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $displayRepository;

  /**
   * The date formatter interface.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The renderer interface.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->displayRepository = $this->container->get('entity_display.repository');
    $this->dateFormatter = $this->container->get('date.formatter');
    $this->renderer = $this->container->get('renderer');

    $this->displayRepository->getViewDisplay('node', 'article')
      ->setComponent($this->fieldName, [
        'type' => 'entity_visibility_preview_formatter',
      ])
      ->save();

    $taxonomy = Vocabulary::create([
      'name' => $this->randomString(),
      'vid' => $this->vocabularyId,
    ]);
    $taxonomy->save();

    $this->prepareDates();
    $this->createTestTaxonomyTerms();
    $this->createTestNodes();
  }

  /**
   * Test the formatter presenting EVP fields value.
   */
  public function testFormatter(): void {
    $this->drupalLogin($this->user);

    $this->nodeDataFormatterTest(1, [
      'Accessible to users with any of those terms:',
      $this->taxonomyTerm1->label(),
    ]);
    $this->nodeDataFormatterTest(2, [
      'Accessible to users with any of those terms:',
      $this->taxonomyTerm2->label(),
    ]);
    $this->nodeDataFormatterTest(3, [
      'Accessible to users with any of those terms:',
      $this->taxonomyTerm1->label(),
      'Accessible from ' . $this->fiveDaysInThePast . ' until ' . $this->fiveDaysInTheFuture . '.',
    ]);
    $this->nodeDataFormatterTest(4, [
      'Accessible to users with any of those terms:',
      $this->taxonomyTerm1->label(),
      'Accessible from ' . $this->fiveDaysInTheFuture . ' until ' . $this->fifteenDaysInTheFuture . '.',
    ]);
    $this->nodeDataFormatterTest(5, [
      'Accessible to users with any of those terms:',
      $this->taxonomyTerm2->label(),
      'Accessible from ' . $this->fiveDaysInThePast . ' until ' . $this->fiveDaysInTheFuture . '.',
    ]);
    $this->nodeDataFormatterTest(6, [
      'Accessible to users with any of those terms:',
      $this->taxonomyTerm2->label(),
      'Accessible from ' . $this->fiveDaysInTheFuture . ' until ' . $this->fifteenDaysInTheFuture . '.',
    ]);
    $this->nodeDataFormatterTest(7, []);
    $this->nodeDataFormatterTest(8, [
      'Accessible from ' . $this->fiveDaysInThePast . '.',
    ]);
    $this->nodeDataFormatterTest(9, [
      'Accessible until ' . $this->fiveDaysInTheFuture . '.',
    ]);
  }

  /**
   * Check that the data are displayed on the node.
   *
   * @param int $nid
   *   The nid to test against.
   * @param array $expectedStrings
   *   All the expected string to find on the node.
   */
  protected function nodeDataFormatterTest(int $nid, array $expectedStrings): void {
    /** @var \Drupal\node\NodeInterface $entity */
    $entity = Node::load($nid);
    $display = $this->displayRepository->getViewDisplay($entity->getEntityTypeId(), $entity->bundle(), 'default');
    $content = $display->build($entity);

    $renderedContent = (string) $this->renderer->renderRoot($content[$this->fieldName]);

    if (empty($expectedStrings)) {
      $this->assertEmpty($renderedContent);
    }
    else {
      foreach ($expectedStrings as $expectedString) {
        $this->assertStringContainsString($expectedString, $renderedContent);
      }
    }
  }

}
