<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\Functional;

use Drupal\node\NodeInterface;

/**
 * Tests for the website preview form.
 *
 * @group entity_visibility_preview
 */
class WebsitePreviewFormTest extends EntityVisibilityPreviewFunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected $userPermissions = [
    'access content',
    'entity_visibility_preview_use_preview',
  ];

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'date_range_condition' => 'date_range_condition',
    ],
  ];

  /**
   * Test the website preview form with the date range plugin.
   */
  public function testWithDateRangePlugin(): void {
    $node = $this->drupalCreateNode([
      'type' => 'article',
      'status' => NodeInterface::PUBLISHED,
      $this->fieldName => \serialize([
        'date_range_condition' => [
          'value' => $this->fiveDaysInThePast,
          'end_value' => $this->fiveDaysInTheFuture,
        ],
      ]),
    ]);

    $this->drupalLogin($this->user);

    $this->datePreviewTest($node, 200, '', FALSE);
    $this->datePreviewTest($node, 403, $this->tenDaysInThePast, FALSE);
    $this->datePreviewTest($node, 200, $this->fiveDaysInThePast, FALSE);
    $this->datePreviewTest($node, 200, $this->today, FALSE);
    $this->datePreviewTest($node, 403, $this->fiveDaysInTheFuture, FALSE);
    $this->datePreviewTest($node, 403, $this->tenDaysInTheFuture, FALSE);
    $this->datePreviewTest($node, 200, '', TRUE);
  }

  /**
   * Helper function.
   *
   * Check status code depending on preview date.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node used for tests.
   * @param int $expected_status_code
   *   The expected status code.
   * @param string $preview_date
   *   The preview date.
   * @param bool $reset
   *   If the preview data should be reset.
   *
   * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
   */
  protected function datePreviewTest(NodeInterface $node, int $expected_status_code = 200, string $preview_date = '', bool $reset = FALSE): void {
    if (!empty($preview_date)) {
      $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
      $this->submitForm([
        'preview_date_display' => $preview_date,
      ], 'Update');
    }
    elseif ($reset) {
      $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
      $this->submitForm([], 'Reset to current date and normal state');
    }

    $this->drupalGet($node->toUrl());
    $this->assertSession()->statusCodeEquals($expected_status_code);
  }

}
