<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\Functional;

use Drupal\node\NodeInterface;

/**
 * Tests for the Date range condition plugin.
 *
 * @group entity_visibility_preview
 */
class DateRangeConditionTest extends EntityVisibilityPreviewFunctionalTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'date_range_condition' => 'date_range_condition',
    ],
  ];

  /**
   * Test the cases of the date range plugin.
   */
  public function testDateRangePlugin(): void {
    $node = $this->drupalCreateNode([
      'type' => 'article',
      'status' => NodeInterface::PUBLISHED,
    ]);

    // Test that the user has access to the node.
    $this->dateRangeTest($node, 200, '', '');

    // Begin date.
    $this->dateRangeTest($node, 200, $this->tenDaysInThePast, '');
    $this->dateRangeTest($node, 200, $this->today, '');
    $this->dateRangeTest($node, 403, $this->tenDaysInTheFuture, '');

    // End date.
    $this->dateRangeTest($node, 403, '', $this->tenDaysInThePast);
    $this->dateRangeTest($node, 403, '', $this->today);
    $this->dateRangeTest($node, 200, '', $this->tenDaysInTheFuture);

    // Date range.
    $this->dateRangeTest($node, 403, $this->tenDaysInThePast, $this->fiveDaysInThePast);
    $this->dateRangeTest($node, 200, $this->tenDaysInThePast, $this->tenDaysInTheFuture);
    $this->dateRangeTest($node, 403, $this->tenDaysInThePast, $this->today);
    $this->dateRangeTest($node, 200, $this->today, $this->tenDaysInTheFuture);
    $this->dateRangeTest($node, 403, $this->fiveDaysInTheFuture, $this->tenDaysInTheFuture);
  }

  /**
   * Helper function.
   *
   * Check status code depending on date conditions.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node used for tests.
   * @param int $expected_status_code
   *   The expected status code.
   * @param string $begin_date
   *   The begin date.
   * @param string $end_date
   *   The end date.
   */
  protected function dateRangeTest(NodeInterface $node, int $expected_status_code = 200, string $begin_date = '', string $end_date = ''): void {
    $node->set($this->fieldName, \serialize([
      'date_range_condition' => [
        'value' => $begin_date,
        'end_value' => $end_date,
      ],
    ]));
    $node->save();
    $this->drupalGet($node->toUrl());
    $this->assertSession()->statusCodeEquals($expected_status_code);
  }

}
