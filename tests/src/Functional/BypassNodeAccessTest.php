<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\Functional;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\user\UserInterface;

/**
 * Validate that the "Bypass access checks" permission is working properly.
 *
 * @group entity_visibility_preview
 */
class BypassNodeAccessTest extends EntityVisibilityPreviewFunctionalTestBase {

  use EntityReferenceFieldCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_visibility_preview_simple_grants',
    'taxonomy',
    'datetime',
    'toolbar',
    'views',
  ];

  /**
   * The user field name containing segmentation.
   *
   * @var string
   */
  protected $userFieldName = 'test_taxonomy_condition';

  /**
   * The vocabulary ID used for the tests.
   *
   * @var string
   */
  protected $vocabularyId = 'taxonomy_condition_test';

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'taxonomy_condition' => 'taxonomy_condition',
      'date_range_condition' => 'date_range_condition',
    ],
    'condition_plugins_settings' => [
      'taxonomy_condition' => [
        'bundles' => [
          'taxonomy_condition_test' => 'taxonomy_condition_test',
        ],
        'user_fields' => [
          'test_taxonomy_condition' => 'test_taxonomy_condition',
        ],
      ],
    ],
  ];

  /**
   * User 1.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user1;

  /**
   * User 2.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user2;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $taxonomy = Vocabulary::create([
      'name' => $this->randomString(),
      'vid' => $this->vocabularyId,
    ]);
    $taxonomy->save();

    $selection_handler_settings = [
      'target_bundles' => [
        $this->vocabularyId => $this->vocabularyId,
      ],
    ];
    $this->createEntityReferenceField('user', 'user', $this->userFieldName, $this->randomString(), 'taxonomy_term', 'default', $selection_handler_settings, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $this->prepareDates();
    $this->createTestTaxonomyTerms();
    $this->createTestNodes();

    $user1 = $this->drupalCreateUser(
      [
        'access content',
        'entity_visibility_preview_use_preview',
        'entity_visibility_preview_bypass_access',
      ],
      $this->randomString(),
      FALSE,
      [
        $this->userFieldName => [
          [
            'target_id' => $this->taxonomyTerm1->id(),
          ],
        ],
      ]
    );
    if (!($user1 instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }
    $this->user1 = $user1;

    $user2 = $this->drupalCreateUser(
      [
        'access content',
        'entity_visibility_preview_use_preview',
      ],
      $this->randomString(),
      FALSE,
      [
        $this->userFieldName => [
          [
            'target_id' => $this->taxonomyTerm1->id(),
          ],
        ],
      ]
    );
    if (!($user2 instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }
    $this->user2 = $user2;
  }

  /**
   * Test that the "Bypass access checks" permission works when not in preview.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testBypassAccessPermission(): void {
    // In normal view, the user with the "Bypass access checks" permission can
    // see all the nodes.
    $this->drupalLogin($this->user1);
    $this->checkAccessVisibility(TRUE, '/node/1');
    $this->checkAccessVisibility(TRUE, '/node/2');
    $this->checkAccessVisibility(TRUE, '/node/3');
    $this->checkAccessVisibility(TRUE, '/node/4');
    $this->checkAccessVisibility(TRUE, '/node/5');
    $this->checkAccessVisibility(TRUE, '/node/6');
    $this->checkAccessVisibility(TRUE, '/node/7');

    // In normal view, the user without the "Bypass access checks" permission
    // has the EVP access check.
    $this->drupalLogin($this->user2);
    $this->checkAccessVisibility(TRUE, '/node/1');
    $this->checkAccessVisibility(FALSE, '/node/2');
    $this->checkAccessVisibility(TRUE, '/node/3');
    $this->checkAccessVisibility(FALSE, '/node/4');
    $this->checkAccessVisibility(FALSE, '/node/5');
    $this->checkAccessVisibility(FALSE, '/node/6');
    $this->checkAccessVisibility(TRUE, '/node/7');

    // In preview, the user with the "Bypass access checks" permission
    // has the EVP access check.
    $this->drupalLogin($this->user1);
    $this->previewChecks();

    // In preview, the user without the "Bypass access checks" permission
    // has the EVP access check. Same result has the user with the permission.
    $this->drupalLogin($this->user2);
    $this->previewChecks();
  }

  /**
   * Test if the user can see the node.
   *
   * @param bool $shouldSee
   *   If the user should see the page or not.
   * @param string $nodeUrl
   *   The URL of the node to test.
   * @param string $previewDate
   *   The preview date.
   * @param string $previewTaxonomy
   *   The preview taxonomy term.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function checkAccessVisibility(
    bool $shouldSee,
    string $nodeUrl,
    string $previewDate = '',
    string $previewTaxonomy = '',
  ): void {
    // Enable preview.
    if (!empty($previewDate) || !empty($previewTaxonomy)) {
      $edit = [
        'preview_date_display' => $previewDate,
        'preview_taxonomy' => $previewTaxonomy,
      ];

      $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
      $this->submitForm($edit, 'Update');
    }

    $this->drupalGet($nodeUrl);
    if ($shouldSee) {
      $this->assertSession()->statusCodeEquals(200);
    }
    else {
      $this->assertSession()->statusCodeEquals(403);
    }
  }

  /**
   * In preview both users should have the same result.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  protected function previewChecks(): void {
    $this->checkAccessVisibility(TRUE, '/node/1', $this->today, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/1', $this->today, 'Segment 2 (2)');
    $this->checkAccessVisibility(TRUE, '/node/1', $this->tenDaysInTheFuture, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/1', $this->tenDaysInTheFuture, 'Segment 2 (2)');

    $this->checkAccessVisibility(FALSE, '/node/2', $this->today, 'Segment 1 (1)');
    $this->checkAccessVisibility(TRUE, '/node/2', $this->today, 'Segment 2 (2)');
    $this->checkAccessVisibility(FALSE, '/node/2', $this->tenDaysInTheFuture, 'Segment 1 (1)');
    $this->checkAccessVisibility(TRUE, '/node/2', $this->tenDaysInTheFuture, 'Segment 2 (2)');

    $this->checkAccessVisibility(TRUE, '/node/3', $this->today, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/3', $this->today, 'Segment 2 (2)');
    $this->checkAccessVisibility(FALSE, '/node/3', $this->tenDaysInTheFuture, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/3', $this->tenDaysInTheFuture, 'Segment 2 (2)');

    $this->checkAccessVisibility(FALSE, '/node/4', $this->today, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/4', $this->today, 'Segment 2 (2)');
    $this->checkAccessVisibility(TRUE, '/node/4', $this->tenDaysInTheFuture, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/4', $this->tenDaysInTheFuture, 'Segment 2 (2)');

    $this->checkAccessVisibility(FALSE, '/node/5', $this->today, 'Segment 1 (1)');
    $this->checkAccessVisibility(TRUE, '/node/5', $this->today, 'Segment 2 (2)');
    $this->checkAccessVisibility(FALSE, '/node/5', $this->tenDaysInTheFuture, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/5', $this->tenDaysInTheFuture, 'Segment 2 (2)');

    $this->checkAccessVisibility(FALSE, '/node/6', $this->today, 'Segment 1 (1)');
    $this->checkAccessVisibility(FALSE, '/node/6', $this->today, 'Segment 2 (2)');
    $this->checkAccessVisibility(FALSE, '/node/6', $this->tenDaysInTheFuture, 'Segment 1 (1)');
    $this->checkAccessVisibility(TRUE, '/node/6', $this->tenDaysInTheFuture, 'Segment 2 (2)');

    $this->checkAccessVisibility(TRUE, '/node/7', $this->today, 'Segment 1 (1)');
    $this->checkAccessVisibility(TRUE, '/node/7', $this->today, 'Segment 2 (2)');
    $this->checkAccessVisibility(TRUE, '/node/7', $this->tenDaysInTheFuture, 'Segment 1 (1)');
    $this->checkAccessVisibility(TRUE, '/node/7', $this->tenDaysInTheFuture, 'Segment 2 (2)');
  }

}
