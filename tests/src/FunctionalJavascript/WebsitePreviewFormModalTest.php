<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\FunctionalJavascript;

/**
 * Performs tests using the modal website preview form.
 *
 * @group entity_visibility_preview
 */
class WebsitePreviewFormModalTest extends EntityVisibilityPreviewFunctionalJavascriptTestBase {

  /**
   * Test the website preview form in a modal dialog.
   *
   * Use the date range condition plugin.
   */
  public function testWebsitePreviewFormModal(): void {
    $this->drupalLogin($this->user);

    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert */
    $assert = $this->assertSession();

    // Test that the links in the toolbar are present.
    $assert->linkExists('Website preview');
    $assert->linkExists('Website preview form');

    // Click on the first level link to display the Entity visibility preview
    // links.
    $this->getSession()->getPage()->clickLink('Website preview');
    // Test that a modal is displayed when clicking on the link.
    $this->getSession()->getPage()->clickLink('Website preview form');

    // Clicking the link triggers a AJAX request/response.
    // Opens a Dialog panel.
    $website_preview_form_dialog_div = $assert->waitForElementVisible('css', 'div.ui-dialog');
    $this->assertNotNull($website_preview_form_dialog_div, 'Link was used to open a dialog (modal)');

    // Test the content of the modal's header.
    $dialog_title = $website_preview_form_dialog_div->find('css', "span.ui-dialog-title:contains('Website preview form')");
    $this->assertNotNull($dialog_title);

    // Test the form button in the modal's footer.
    $update_button = $website_preview_form_dialog_div->findButton('Update');
    $this->assertNotNull($update_button);
    $reset_button = $website_preview_form_dialog_div->findButton('Reset to current date and normal state');
    $this->assertNotNull($reset_button);
  }

}
