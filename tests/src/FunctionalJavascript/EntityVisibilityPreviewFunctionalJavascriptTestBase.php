<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\entity_visibility_preview\Traits\EntityVisibilityPreviewTestHelperTrait;
use Drupal\user\UserInterface;

/**
 * Provides helper methods for the EVP module's functional javascript tests.
 */
abstract class EntityVisibilityPreviewFunctionalJavascriptTestBase extends WebDriverTestBase {

  use EntityVisibilityPreviewTestHelperTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_visibility_preview',
    'node',
    'toolbar',
  ];

  /**
   * The list of user permissions.
   *
   * @var array
   */
  protected $userPermissions = [
    'access toolbar',
    'entity_visibility_preview_use_preview',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create article node type.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType([
        'type' => 'article',
        'name' => 'Article',
      ]);
    }

    $this->createFieldStorage();
    $this->createFieldConfig();

    $user = $this->drupalCreateUser($this->userPermissions);
    if (!($user instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }
    $this->user = $user;
  }

}
