<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\FunctionalJavascript;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Tests for the website preview message.
 *
 * @group entity_visibility_preview
 */
class WebsitePreviewMessageTest extends EntityVisibilityPreviewFunctionalJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
  ];

  /**
   * {@inheritdoc}
   */
  protected $userPermissions = [
    'access content',
    'entity_visibility_preview_use_preview',
  ];

  /**
   * Test the website preview message, reset button and settings.
   */
  public function testPreviewMessageAndReset(): void {
    // phpcs:disable
    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = $this->container->get('date.formatter');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->container->get('config.factory');

    $date_for_settings = $date_formatter->format(\strtotime('-5 days'), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT);
    $date_for_form = $date_formatter->format(\strtotime('-5 days'), 'custom', 'm/d/Y');

    $this->drupalLogin($this->user);

    // Select a date to preview.
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert */
    $assert = $this->assertSession();
    $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
    // The language is "en" and so HTML datetime expect this format.
    $assert->fieldExists('preview_date_display')->setValue($date_for_form);
    $assert->buttonExists('Update')->press();

    // @todo fix this test on drupal.org
    //    // Check that the preview message is present. This also check the default
    //    // settings of the module.
    //    $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
    //    $website_preview_message_div = $assert->waitForElementVisible('css', 'div.messages__wrapper div.messages[data-drupal-message-id="entity_visibility_preview"]');
    //    $this->assertNotNull($website_preview_message_div, 'The message is displayed after AJAX request.');
    //
    //    /** @var \Behat\Mink\Element\NodeElement $title */
    //    $title = $website_preview_message_div->find('css', 'h3');
    //    $this->assertEquals($this->t('The website is in preview mode with the further values:'), $title->getText());
    //
    //    /** @var \Behat\Mink\Element\NodeElement $date_element */
    //    $date_element = $website_preview_message_div->find('css', 'ul li');
    //    $this->assertEquals($this->t('Display date: @date', [
    //      '@date' => $date_for_settings,
    //    ]), $date_element->getText());
    //
    //    // Disable displaying the preview message.
    //    $config_factory->getEditable('entity_visibility_preview.settings')
    //      ->set('display_preview_message', FALSE)
    //      ->save();
    //
    //    // Test that the preview message is no more present.
    //    $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
    //    $website_preview_message_div = $assert->waitForElementVisible('css', 'div.messages__wrapper div.messages[data-drupal-message-id="entity_visibility_preview"]');
    //    $this->assertNull($website_preview_message_div, 'The message is not displayed if disabled in settings.');
    //
    //    // Enable displaying preview message.
    //    $config_factory->getEditable('entity_visibility_preview.settings')
    //      ->set('display_preview_message', TRUE)
    //      ->save();
    //
    //    // Check that the preview message is present.
    //    $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
    //    $website_preview_message_div = $assert->waitForElementVisible('css', 'div.messages__wrapper div.messages[data-drupal-message-id="entity_visibility_preview"]');
    //    $this->assertNotNull($website_preview_message_div, 'The message is displayed after AJAX request.');
    //
    //    /** @var \Behat\Mink\Element\NodeElement $title */
    //    $title = $website_preview_message_div->find('css', 'h3');
    //    $this->assertEquals($this->t('The website is in preview mode with the further values:'), $title->getText());
    //
    //    /** @var \Behat\Mink\Element\NodeElement $date_element */
    //    $date_element = $website_preview_message_div->find('css', 'ul li');
    //    $this->assertEquals($this->t('Display date: @date', [
    //      '@date' => $date_for_settings,
    //    ]), $date_element->getText());
    //
    //    // Reset preview.
    //    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert */
    //    $assert = $this->assertSession();
    //    $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
    //    $assert->buttonExists($this->t('Reset to current date and normal state'))->press();
    //
    //    // Check that the preview message is no more present.
    //    $website_preview_message_div = $assert->waitForElementVisible('css', 'div.messages__wrapper div.messages[data-drupal-message-id="entity_visibility_preview"]');
    //    $this->assertNull($website_preview_message_div, 'The message is not displayed after reset.');
    // phpcs:enable
  }

}
