<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\FunctionalJavascript;

use Drupal\node\NodeInterface;

/**
 * Tests that a plugin can be configured in the field instance.
 *
 * @group entity_visibility_preview
 */
class ConfigurablePluginTest extends EntityVisibilityPreviewFunctionalJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field_ui',
    'entity_visibility_preview_configurable_plugin_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $userPermissions = [
    'access content',
    'administer node fields',
  ];

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'configurable_plugin_test' => 'configurable_plugin_test',
    ],
    'condition_plugins_settings' => [
      'configurable_plugin_test' => [
        'allow_access' => FALSE,
      ],
    ],
  ];

  /**
   * Test that the a plugin can have configuration.
   */
  public function testConfigurablePlugin(): void {
    $node = $this->drupalCreateNode([
      'type' => 'article',
      'status' => NodeInterface::PUBLISHED,
      $this->fieldName => \serialize([
        'configurable_plugin_test' => [
          'test' => $this->randomString(),
        ],
      ]),
    ]);

    $this->drupalLogin($this->user);

    // Check that the user do not have access to the node because of the plugin
    // settings.
    $this->drupalGet($node->toUrl());
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert */
    $assert = $this->assertSession();
    $assert->pageTextNotContains((string) $node->label());

    // Go to the field settings.
    $this->drupalGet('/admin/structure/types/manage/article/fields/node.article.field_entity_visibility_preview');
    $assert->checkboxChecked('settings[enabled_condition_plugins][configurable_plugin_test]');
    $this->assertTrue($assert->elementExists('css', '#edit-settings-condition-plugins-settings-configurable-plugin-test')->isVisible(), 'The configuration form of the plugin is visible.');

    // Disable the plugin.
    $assert->fieldExists('settings[enabled_condition_plugins][configurable_plugin_test]')->click();
    $assert->checkboxNotChecked('settings[enabled_condition_plugins][configurable_plugin_test]');
    $this->assertFalse($assert->elementExists('css', '#edit-settings-condition-plugins-settings-configurable-plugin-test')->isVisible(), 'The configuration form of the plugin is hidden.');

    // Enable the plugin and change the value.
    $assert->fieldExists('settings[enabled_condition_plugins][configurable_plugin_test]')->click();
    $assert->checkboxChecked('settings[enabled_condition_plugins][configurable_plugin_test]');
    $configurable_plugin_test_form = $assert->elementExists('css', '#edit-settings-condition-plugins-settings-configurable-plugin-test');
    /** @var \Behat\Mink\Element\NodeElement $configurable_plugin_test_form_checkbox */
    $configurable_plugin_test_form_checkbox = $configurable_plugin_test_form->findField('settings[condition_plugins_settings][configurable_plugin_test][allow_access]');
    $this->assertTrue($configurable_plugin_test_form->isVisible(), 'The configuration form of the plugin is visible.');
    $this->assertFalse($configurable_plugin_test_form_checkbox->isVisible(), 'The checkbox of the configuration form of the plugin is hidden.');

    // Expand plugin configuration form details.
    /** @var \Behat\Mink\Element\NodeElement $summary_element */
    $summary_element = $configurable_plugin_test_form->find('css', 'summary');
    $summary_element->click();

    $this->assertTrue($configurable_plugin_test_form_checkbox->isVisible(), 'The checkbox of the configuration form of the plugin is visible.');
    $this->assertFalse($configurable_plugin_test_form_checkbox->isChecked(), 'The allow access checkbox is unchecked as it is the default configuration for the test');
    $configurable_plugin_test_form_checkbox->check();
    $this->assertTrue($configurable_plugin_test_form_checkbox->isChecked(), 'The allow access checkbox is checked');

    // Save the form.
    $assert->buttonExists('Save settings')->press();

    // Check that the user now have access to the the node.
    $this->drupalGet($node->toUrl());
    /** @var \Drupal\FunctionalJavascriptTests\JSWebAssert $assert */
    $assert = $this->assertSession();
    $assert->pageTextContains((string) $node->label());
  }

}
