<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests module dependency.
 *
 * @group entity_visibility_preview
 */
class DependencyTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_visibility_preview',
    'entity_visibility_preview_plugin_dependency_test',
  ];

  /**
   * Test that a plugin missing a module dependency is not detected.
   */
  public function testDependency(): void {
    /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager $entity_visibility_preview_manager */
    $entity_visibility_preview_manager = $this->container->get('plugin.manager.entity_visibility_preview_condition');
    $definitions = $entity_visibility_preview_manager->getDefinitions();
    $this->assertFalse(isset($definitions['dependency_test']), 'A plugin with a missing dependency is not detected.');
  }

}
