<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview\Traits;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides common methods to tests.
 */
trait EntityVisibilityPreviewTestHelperTrait {

  /**
   * The field name.
   *
   * @var string
   */
  protected $fieldName = 'field_entity_visibility_preview';

  /**
   * Test user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * Test taxonomy term.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $taxonomyTerm1;

  /**
   * Test taxonomy term.
   *
   * @var \Drupal\taxonomy\TermInterface
   */
  protected $taxonomyTerm2;

  /**
   * A formatted date to use with EVP.
   *
   * @var string
   */
  protected $tenDaysInThePast;

  /**
   * A formatted date to use with EVP.
   *
   * @var string
   */
  protected $fiveDaysInThePast;

  /**
   * A formatted date to use with EVP.
   *
   * @var string
   */
  protected $today;

  /**
   * A formatted date to use with EVP.
   *
   * @var string
   */
  protected $fiveDaysInTheFuture;

  /**
   * A formatted date to use with EVP.
   *
   * @var string
   */
  protected $tenDaysInTheFuture;

  /**
   * A formatted date to use with EVP.
   *
   * @var string
   */
  protected $fifteenDaysInTheFuture;

  /**
   * Helper function to prepare dates to use in tests.
   */
  protected function prepareDates(): void {
    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = $this->container->get('date.formatter');
    $this->tenDaysInThePast = $date_formatter->format(\strtotime('-10 days'), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT);
    $this->fiveDaysInThePast = $date_formatter->format(\strtotime('-5 days'), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT);
    $this->today = $date_formatter->format(\strtotime('today'), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT);
    $this->fiveDaysInTheFuture = $date_formatter->format(\strtotime('+5 days'), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT);
    $this->tenDaysInTheFuture = $date_formatter->format(\strtotime('+10 days'), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT);
    $this->fifteenDaysInTheFuture = $date_formatter->format(\strtotime('+15 days'), 'custom', DateTimeItemInterface::DATE_STORAGE_FORMAT);
  }

  /**
   * Helper function to create the field storage.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFieldStorage(): void {
    FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'node',
      'type' => 'entity_visibility_preview',
      'settings' => [],
      'cardinality' => 1,
    ])->save();
  }

  /**
   * Helper function to create the field config.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createFieldConfig(): void {
    $field_config = FieldConfig::create([
      'field_name' => $this->fieldName,
      'label' => 'Entity visibility preview',
      'entity_type' => 'node',
      'bundle' => 'article',
      'required' => FALSE,
      'settings' => $this->fieldSettings ?? [],
    ]);
    $field_config->save();
  }

  /**
   * Helper function to create taxonomy terms used in tests.
   */
  protected function createTestTaxonomyTerms(): void {
    $taxonomy_term_1 = Term::create([
      'name' => 'Segment 1',
      // @phpstan-ignore-next-line
      'vid' => $this->vocabularyId,
      'uuid' => 'segment1_uuid',
    ]);
    $taxonomy_term_1->save();
    $this->taxonomyTerm1 = $taxonomy_term_1;

    $taxonomy_term_2 = Term::create([
      'name' => 'Segment 2',
      // @phpstan-ignore-next-line
      'vid' => $this->vocabularyId,
      'uuid' => 'segment2_uuid',
    ]);
    $taxonomy_term_2->save();
    $this->taxonomyTerm2 = $taxonomy_term_2;
  }

  /**
   * Helper function to create content used for tests.
   */
  protected function createTestNodes(): void {
    $nodes = [
      [
        'type' => 'article',
        'title' => 'Article 1 - Segment: 1 - Date: any',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [$this->taxonomyTerm1->uuid()],
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 2 - Segment: 2 - Date: any',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [$this->taxonomyTerm2->uuid()],
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 3 - Segment: 1 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [$this->taxonomyTerm1->uuid()],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 4 - Segment: 1 - Date: +5 days to +15 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [$this->taxonomyTerm1->uuid()],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInTheFuture,
            'end_value' => $this->fifteenDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 5 - Segment: 2 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [$this->taxonomyTerm2->uuid()],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 6 - Segment: 2 - Date: +5 days to +15 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [$this->taxonomyTerm2->uuid()],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInTheFuture,
            'end_value' => $this->fifteenDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 7 - No evp',
        'status' => NodeInterface::PUBLISHED,
      ],
      [
        'type' => 'article',
        'title' => 'Article 8 - No segments - Date: -5 days ago',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => '',
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 9 - No segments - Date: until +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'date_range_condition' => [
            'value' => '',
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
    ];

    foreach ($nodes as $node) {
      $this->drupalCreateNode($node);
    }
    \node_access_rebuild();
  }

}
