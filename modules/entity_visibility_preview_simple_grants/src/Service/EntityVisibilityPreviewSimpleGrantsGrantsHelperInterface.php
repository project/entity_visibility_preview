<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview_simple_grants\Service;

use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Interface for the EntityVisibilityPreviewSimpleGrantsGrantsHelper service.
 */
interface EntityVisibilityPreviewSimpleGrantsGrantsHelperInterface {

  public const LOGGER_CHANNEL = 'entity_visibility_preview_simple_grants_grants_helper';

  /**
   * Simple grants table that stores "grants" for all nodes.
   */
  public const EVP_SIMPLE_GRANTS_TABLE = 'entity_visibility_preview_simple_grants';

  /**
   * Returns node "grants" saved for given node id.
   *
   * @param int $nodeId
   *   The node id to retrieve grants for.
   *
   * @return array
   *   An array of grant IDs
   */
  public function getNodeGrantsIds(int $nodeId): array;

  /**
   * Deletes node "grants" for given node id.
   *
   * @param int $nodeId
   *   The node id to remove grants for.
   *
   * @return int
   *   The number of rows deleted.
   */
  public function deleteNodeGrants(int $nodeId): int;

  /**
   * Inserts given grants.
   *
   * The grantDefinitions array is an array of arrays, that must have 4 columns:
   *   entity_id (int)
   *   date_start (timestamp)
   *   date_end (timestamp)
   *   segment (id int)
   *
   * @param array $grantDefinitions
   *   An array of grant definitions as specified in method description.
   *
   * @return int
   *   The number of inserted rows.
   */
  public function insertNodeGrants(array $grantDefinitions): int;

  /**
   * Computes node grants (according to EVP fields) and save them.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to compute grants for.
   *
   * @return int
   *   The number of grants inserted.
   */
  public function computeNodeGrantsAndSave(NodeInterface $node): int;

  /**
   * Returns user grants according to taxonomies, current date.
   *
   * Also takes into account the preview form if used.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Account to check for.
   *
   * @return array
   *   An array of grant ids.
   */
  public function getUserGrants(AccountInterface $account): array;

  /**
   * Returns an array of grant ids matching conditions for segment and dates.
   *
   * The conditions are:
   * (segment is null OR in $segments) AND (date_start is null OR date_start
   * <= $date) AND (date_end is null OR date_end > $date)
   *
   * @param array $segments
   *   An array of user segments.
   * @param int $date
   *   Date timestamp.
   *
   * @return array
   *   Array of gids.
   */
  public function getGrantsFromConditions(array $segments, int $date): array;

}
