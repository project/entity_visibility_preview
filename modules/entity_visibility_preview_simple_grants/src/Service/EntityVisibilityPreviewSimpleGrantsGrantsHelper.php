<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview_simple_grants\Service;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager;
use Drupal\entity_visibility_preview\Service\EntityVisibilityPreviewFieldHelperInterface;
use Drupal\entity_visibility_preview\Service\EntityVisibilityPreviewTaxonomyHelperInterface;
use Drupal\node\NodeInterface;

/**
 * Global configuration for Entity visibility preview taxonomy condition.
 */
class EntityVisibilityPreviewSimpleGrantsGrantsHelper implements EntityVisibilityPreviewSimpleGrantsGrantsHelperInterface {

  /**
   * Database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The entity visibility preview condition plugin manager.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * Entity visibility preview field helper.
   *
   * @var \Drupal\entity_visibility_preview\Service\EntityVisibilityPreviewFieldHelperInterface
   */
  protected $entityVisibilityPreviewFieldHelper;

  /**
   * Taxonomy helper.
   *
   * @var \Drupal\entity_visibility_preview\Service\EntityVisibilityPreviewTaxonomyHelperInterface
   */
  protected $entityVisibilityPreviewTaxonomyHelper;

  /**
   * EntityVisibilityPreviewSimpleGrantsGrantsHelper constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel factory.
   * @param \Drupal\entity_visibility_preview\Service\EntityVisibilityPreviewFieldHelperInterface $entityVisibilityPreviewFieldHelper
   *   Entity visibility preview field helper.
   * @param \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager $entity_visibility_preview_condition_plugin_manager
   *   The entity visibility preview condition plugin manager.
   * @param \Drupal\entity_visibility_preview\Service\EntityVisibilityPreviewTaxonomyHelperInterface $entityVisibilityPreviewTaxonomyHelper
   *   The entity visibility preview taxonomy helper.
   */
  public function __construct(
    Connection $connection,
    LoggerChannelFactoryInterface $loggerChannelFactory,
    EntityVisibilityPreviewFieldHelperInterface $entityVisibilityPreviewFieldHelper,
    EntityVisibilityPreviewConditionPluginManager $entity_visibility_preview_condition_plugin_manager,
    EntityVisibilityPreviewTaxonomyHelperInterface $entityVisibilityPreviewTaxonomyHelper,
  ) {
    $this->connection = $connection;
    $this->logger = $loggerChannelFactory->get(self::LOGGER_CHANNEL);
    $this->entityVisibilityPreviewFieldHelper = $entityVisibilityPreviewFieldHelper;
    $this->entityVisibilityPreviewConditionPluginManager = $entity_visibility_preview_condition_plugin_manager;
    $this->entityVisibilityPreviewTaxonomyHelper = $entityVisibilityPreviewTaxonomyHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeGrantsIds(int $nodeId): array {
    $selectResult = $this->connection->select(self::EVP_SIMPLE_GRANTS_TABLE, 'evp')
      ->fields('evp', ['gid'])
      ->condition('entity_id', (string) $nodeId, '=')
      ->execute();

    if ($selectResult) {
      return $selectResult->fetchCol();
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function deleteNodeGrants(int $nodeId): int {
    return $this->connection->delete(self::EVP_SIMPLE_GRANTS_TABLE)
      ->condition('entity_id', (string) $nodeId, '=')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function insertNodeGrants(array $grantDefinitions): int {
    $result = 0;
    try {
      $query = $this->connection->insert(self::EVP_SIMPLE_GRANTS_TABLE)
        ->fields(['entity_id', 'date_start', 'date_end', 'segment']);

      foreach ($grantDefinitions as $grantDefinition) {
        $query->values($grantDefinition);
      }
      $result = $query->execute();
    }
    catch (\Exception $exception) {
      $this->logger->error('An error occurred trying to save grants: {exception}. Values were: {json}', [
        'exception' => $exception->getMessage(),
        'json' => \json_encode($grantDefinitions),
      ]);
    }
    return $result ? 1 : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function computeNodeGrantsAndSave(NodeInterface $node): int {
    $nbGrants = 0;
    $fields = $this->entityVisibilityPreviewFieldHelper->getEntityVisibilityPreviewFieldsForBundle('node', $node->bundle());

    // Delete all grants for this node to save new ones.
    $this->deleteNodeGrants((int) $node->id());

    // Get values configured for every EVP field of the node.
    foreach (\array_keys($fields) as $fieldName) {
      $field = $node->get($fieldName);
      if (!$field->isEmpty()) {
        /** @var array $field_value */
        $field_value = $field->getValue();
        /** @var array $entity_conditions */
        $entity_conditions = \unserialize($field_value[0]['value'], [
          'allowed_classes' => FALSE,
        ]);
        $start_date = $end_date = NULL;
        $grants = [];
        if (!empty($entity_conditions['date_range_condition']['value'])) {
          $start_date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $entity_conditions['date_range_condition']['value'])
            ->setTime(0, 0, 0)
            ->getTimestamp();
        }
        if (!empty($entity_conditions['date_range_condition']['end_value'])) {
          $end_date = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $entity_conditions['date_range_condition']['end_value'])
            ->setTime(0, 0, 0)
            ->getTimestamp();
        }
        $taxonomyIds = !empty($entity_conditions['taxonomy_condition']) ?
          $this->entityVisibilityPreviewTaxonomyHelper->getTidsFromUuids($entity_conditions['taxonomy_condition']['taxonomy_term_uuids']) : [NULL];
        foreach ($taxonomyIds as $taxonomyId) {
          $grants[] = [
            'entity_id' => $node->id(),
            'date_start' => $start_date,
            'date_end' => $end_date,
            'segment' => $taxonomyId,
          ];
        }
        $nbGrants += $this->insertNodeGrants($grants);
      }
    }

    return (int) $nbGrants;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserGrants(AccountInterface $account): array {
    $userTaxonomies = [];
    $userGrants = [];

    try {
      // We have to go over every EVP field to retrieve taxonomies, since the
      // vocabularies used on user account are configurable at a plugin level.
      // It is either that, or we go over all taxonomy reference fields on users
      // but it will lead to potentially more taxonomy terms than strictly
      // "necessary", because vocabularies not used on EVP fields are not
      // relevant.
      $evpFields = $this->entityVisibilityPreviewFieldHelper->getAllEntityVisibilityPreviewFields();
      foreach ($evpFields as $evpFieldInfos) {
        foreach ($evpFieldInfos as $evpFieldInfo) {
          foreach ($evpFieldInfo as $fieldInfos) {
            // For every field of type EVP, if taxonomy condition is enabled,
            // retrieve taxonomy terms for given user (or taxonomies configured
            // on preview form if set, according to the return of the plugin).
            if (!empty($fieldInfos['enabled_plugins']['taxonomy_condition']) && $this->entityVisibilityPreviewConditionPluginManager->hasDefinition('taxonomy_condition')) {
              $condition_plugins_settings = $fieldInfos['field_definition']->getSetting('condition_plugins_settings');
              $configuration = $condition_plugins_settings['taxonomy_condition'] ?? [];
              /** @var \Drupal\entity_visibility_preview\Plugin\EntityVisibilityPreviewCondition\TaxonomyCondition $entityVisibilityPreviewCondition */
              $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance('taxonomy_condition', $configuration);
              $userTaxonomies = \array_merge($userTaxonomies, $entityVisibilityPreviewCondition->getActualTaxonomy($fieldInfos['field_definition'], $account));
            }
          }
        }
      }
      $currentDate = new DrupalDateTime('today');
      $currentDate = $currentDate
        ->setTime(0, 0, 0)
        ->getTimestamp();
      if ($this->entityVisibilityPreviewConditionPluginManager->hasDefinition('date_range_condition')) {
        /** @var \Drupal\entity_visibility_preview\Plugin\EntityVisibilityPreviewCondition\DateRangeCondition $datePlugin */
        $datePlugin = $this->entityVisibilityPreviewConditionPluginManager->createInstance('date_range_condition', []);
        $currentDate = $datePlugin->getActualTimestamp();
      }

      $userGrants = $this->getGrantsFromConditions($userTaxonomies, $currentDate);
    }
    catch (\Exception $exception) {
      $this->logger->error('An error occurred while trying to retrieve user grants: {exception}. User account: {userId}', [
        'exception' => $exception->getMessage(),
        'userId' => $account->id(),
      ]);
    }
    return $userGrants;
  }

  /**
   * {@inheritdoc}
   */
  public function getGrantsFromConditions(array $segments, int $date): array {
    $query = $this->connection->select(self::EVP_SIMPLE_GRANTS_TABLE, 'evpsg')
      ->fields('evpsg', ['gid']);

    if (!empty($segments)) {
      $query->condition(
        $query->orConditionGroup()
          ->condition('segment', $segments, 'IN')
          ->isNull('segment')
      );
    }
    else {
      $query->isNull('segment');
    }

    $query->condition(
      $query->orConditionGroup()
        ->condition('date_start', (string) $date, '<=')
        ->isNull('date_start')
    );

    $query->condition(
      $query->orConditionGroup()
        ->condition('date_end', (string) $date, '>')
        ->isNull('date_end')
    );

    $queryResult = $query->execute();

    if ($queryResult) {
      return $queryResult->fetchCol();
    }

    return [];
  }

}
