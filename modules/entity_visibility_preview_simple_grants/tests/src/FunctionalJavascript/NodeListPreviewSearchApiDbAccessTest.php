<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview_simple_grants\FunctionalJavascript;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Performs tests to validate grants are working properly with preview.
 *
 * /!\ Caching of search_api views causes issues when changing user grants.
 * The content displayed is not updated, although if works well with "normal"
 * views.
 *
 * @group entity_visibility_preview
 * @group entity_visibility_preview_simple_grants
 */
class NodeListPreviewSearchApiDbAccessTest extends NodeListPreviewAccessTest {

  use StringTranslationTrait;

  /**
   * Url to display using test browser.
   *
   * @var string
   */
  protected $testUrlToDisplay = '/test-search-api-db';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_visibility_preview',
    'entity_visibility_preview_simple_grants',
    'evp_simple_grants_search_api_db_test',
    'node',
    'taxonomy',
    'toolbar',
    'views',
  ];

  /**
   * {@inheritdoc}
   */
  public function testNodeListPreviewAccessTest(): void {
    /** @var \Drupal\search_api\IndexInterface $index */
    $index = \Drupal::entityTypeManager()->getStorage('search_api_index')->load('index');
    $index->indexItems();
    parent::testNodeListPreviewAccessTest();
  }

}
