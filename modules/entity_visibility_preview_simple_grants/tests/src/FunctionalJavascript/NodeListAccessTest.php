<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview_simple_grants\FunctionalJavascript;

/**
 * Performs tests to validate grants are working properly.
 *
 * @group entity_visibility_preview
 * @group entity_visibility_preview_simple_grants
 */
class NodeListAccessTest extends EntityVisibilityPreviewListFunctionalJavascriptTestBase {

  /**
   * Test that the list of nodes displays only accessible nodes.
   *
   * Uses the date range and taxonomy condition plugins.
   */
  public function testNodeListAccessTest(): void {
    $this->drupalLogin($this->user);

    // Access the "node list" page and check what nodes are displayed.
    $this->drupalGet($this->testUrlToDisplay);

    $this->assertSession()->pageTextContains('Article 1 - Segment: 1 - Date: any');
    $this->assertSession()->pageTextNotContains('Article 2 - Segment: 2 - Date: any');
    $this->assertSession()->pageTextContains('Article 3 - Segment: 1 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextNotContains('Article 4 - Segment: 1 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextNotContains('Article 5 - Segment: 2 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextNotContains('Article 6 - Segment: 2 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextContains('Article 7 - No evp');
  }

}
