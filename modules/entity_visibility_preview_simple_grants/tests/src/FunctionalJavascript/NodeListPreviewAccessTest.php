<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview_simple_grants\FunctionalJavascript;

/**
 * Performs tests to validate grants are working properly with preview.
 *
 * @group entity_visibility_preview
 * @group entity_visibility_preview_simple_grants
 */
class NodeListPreviewAccessTest extends EntityVisibilityPreviewListFunctionalJavascriptTestBase {

  /**
   * Test that the list of nodes displays only accessible nodes using preview.
   *
   * Uses the date range and taxonomy condition plugins.
   */
  public function testNodeListPreviewAccessTest(): void {
    $this->drupalLogin($this->user);
    $this->initSessionPreview();

    // Access the "node list" page and check what nodes are displayed.
    $this->drupalGet($this->testUrlToDisplay);

    $this->assertSession()->pageTextNotContains('Article 1 - Segment: 1 - Date: any');
    $this->assertSession()->pageTextContains('Article 2 - Segment: 2 - Date: any');
    $this->assertSession()->pageTextNotContains('Article 3 - Segment: 1 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextNotContains('Article 4 - Segment: 1 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextNotContains('Article 5 - Segment: 2 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextContains('Article 6 - Segment: 2 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextContains('Article 7 - No evp');
  }

  /**
   * Inits preview session to a date in the future and a segment.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function initSessionPreview(): void {
    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = $this->container->get('date.formatter');
    $date_for_form = $date_formatter->format(\strtotime('+7 days'), 'custom', 'm/d/Y');

    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['uuid' => 'segment2_uuid']);
    /** @var \Drupal\taxonomy\TermInterface $segment2 */
    $segment2 = \reset($terms);
    $this->assertNotNull($segment2);

    // Select a date to preview.
    $assert = $this->assertSession();
    $this->drupalGet('/admin/content/entity_visibility_preview/preview_form');
    // The language is "en" and so HTML datetime expect this format.
    $assert->fieldExists('preview_taxonomy')->setValue($segment2->label() . ' (' . $segment2->id() . ')');
    $assert->fieldExists('preview_date_display')->setValue($date_for_form);
    $assert->buttonExists('Update')->press();
  }

}
