<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview_simple_grants\FunctionalJavascript;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\entity_visibility_preview\Traits\EntityVisibilityPreviewTestHelperTrait;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\user\UserInterface;

/**
 * Provides helper methods for the EVP module's functional javascript tests.
 */
abstract class EntityVisibilityPreviewListFunctionalJavascriptTestBase extends WebDriverTestBase {

  use EntityVisibilityPreviewTestHelperTrait;
  use EntityReferenceFieldCreationTrait;

  /**
   * Url to display using test browser.
   *
   * @var string
   */
  protected $testUrlToDisplay = '/node';

  /**
   * The user field name containing segmentation.
   *
   * @var string
   */
  protected $userFieldName = 'test_taxonomy_condition';

  /**
   * The vocabulary ID used for the tests.
   *
   * @var string
   */
  protected $vocabularyId = 'taxonomy_condition_test';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'taxonomy_condition' => 'taxonomy_condition',
      'date_range_condition' => 'date_range_condition',
    ],
    'condition_plugins_settings' => [
      'taxonomy_condition' => [
        'bundles' => [
          'taxonomy_condition_test' => 'taxonomy_condition_test',
        ],
        'user_fields' => [
          'test_taxonomy_condition' => 'test_taxonomy_condition',
        ],
      ],
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_visibility_preview',
    'entity_visibility_preview_simple_grants',
    'node',
    'taxonomy',
    'toolbar',
    'views',
  ];

  /**
   * The list of user permissions.
   *
   * @var array
   */
  protected $userPermissions = [
    'access toolbar',
    'entity_visibility_preview_use_preview',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create article node type.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType([
        'type' => 'article',
        'name' => 'Article',
      ]);
    }

    $this->createFieldStorage();
    $this->createFieldConfig();

    $taxonomy = Vocabulary::create([
      'name' => $this->randomString(),
      'vid' => $this->vocabularyId,
    ]);
    $taxonomy->save();

    $selection_handler_settings = [
      'target_bundles' => [
        $this->vocabularyId => $this->vocabularyId,
      ],
    ];
    $this->createEntityReferenceField('user', 'user', $this->userFieldName, $this->randomString(), 'taxonomy_term', 'default', $selection_handler_settings, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);

    $this->prepareDates();
    $this->createTestTaxonomyTerms();
    $this->createTestNodes();

    $user = $this->drupalCreateUser(
      $this->userPermissions,
      $this->randomString(),
      FALSE,
      [
        $this->userFieldName => [
          [
            'target_id' => $this->taxonomyTerm1->id(),
          ],
        ],
      ]
    );
    if (!($user instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }
    $this->user = $user;
  }

}
