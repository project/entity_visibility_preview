<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview_simple_grants\FunctionalJavascript;

/**
 * Performs tests to validate grants are working properly with >1 segments.
 *
 * When more than 1 segment is selected, the taxonomy grant condition works
 * as an "OR" condition.
 *
 * @group entity_visibility_preview
 * @group entity_visibility_preview_simple_grants
 */
class NodeListMultipleSegmentAccessTest extends EntityVisibilityPreviewListFunctionalJavascriptTestBaseMultipleSegments {

  /**
   * Test that the list of nodes displays only accessible nodes.
   *
   * Uses the date range and taxonomy condition plugins.
   */
  public function testNodeListAccessTest(): void {
    $this->drupalLogin($this->user);

    // Access the "node list" page and check what nodes are displayed.
    $this->drupalGet($this->testUrlToDisplay);

    $this->assertSession()->pageTextContains('Article 1 - Segment: 1|Subsegment: 1 - Date: any');
    $this->assertSession()->pageTextContains('Article 2 - Segment: 2|Subsegment: 1 - Date: any');
    $this->assertSession()->pageTextContains('Article 3 - Segment: 1|Subsegment: 2 - Date: any');
    $this->assertSession()->pageTextNotContains('Article 4 - Segment: 2|Subsegment: 2 - Date: any');
    $this->assertSession()->pageTextContains('Article 5 - Segment: 1|Subsegment: 1 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextContains('Article 6 - Segment: 2|Subsegment: 1 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextContains('Article 7 - Segment: 1|Subsegment: 2 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextNotContains('Article 8 - Segment: 2|Subsegment: 2 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextNotContains('Article 9 - Segment: 1|Subsegment: 1 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextNotContains('Article 10 - Segment: 2|Subsegment: 1 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextNotContains('Article 11 - Segment: 1|Subsegment: 2 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextNotContains('Article 12 - Segment: 2|Subsegment: 2 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextContains('Article 13 - Segment: 1 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextNotContains('Article 14 - Segment: 2 - Date: -5 days to +5 days');
    $this->assertSession()->pageTextNotContains('Article 15 - Segment: 1 - Date: +5 days to +15 days');
    $this->assertSession()->pageTextContains('Article 16 - No evp');
  }

}
