<?php

declare(strict_types=1);

namespace Drupal\Tests\entity_visibility_preview_simple_grants\FunctionalJavascript;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\entity_visibility_preview\Traits\EntityVisibilityPreviewTestHelperTrait;
use Drupal\Tests\field\Traits\EntityReferenceFieldCreationTrait;
use Drupal\user\UserInterface;

/**
 * Provides helper methods for the EVP module's functional javascript tests.
 *
 * Creates contents with multiple taxonomy vocabularies segmentation.
 */
abstract class EntityVisibilityPreviewListFunctionalJavascriptTestBaseMultipleSegments extends WebDriverTestBase {

  use EntityVisibilityPreviewTestHelperTrait;
  use EntityReferenceFieldCreationTrait;

  /**
   * Url to display using test browser.
   *
   * @var string
   */
  protected $testUrlToDisplay = '/node';

  /**
   * The user field names containing segmentation.
   *
   * @var array
   */
  protected $userFieldNames = [
    'test_taxonomy_condition_1',
    'test_taxonomy_condition_2',
  ];

  /**
   * The vocabulary IDs used for the tests.
   *
   * @var array
   */
  protected $vocabularyIds = [
    'taxonomy_condition_test_1',
    'taxonomy_condition_test_2',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected array $fieldSettings = [
    'enabled_condition_plugins' => [
      'taxonomy_condition' => 'taxonomy_condition',
      'date_range_condition' => 'date_range_condition',
    ],
    'condition_plugins_settings' => [
      'taxonomy_condition' => [
        'bundles' => [
          'taxonomy_condition_test_1' => 'taxonomy_condition_test_1',
          'taxonomy_condition_test_2' => 'taxonomy_condition_test_2',
        ],
        'user_fields' => [
          'test_taxonomy_condition_1' => 'test_taxonomy_condition_1',
          'test_taxonomy_condition_2' => 'test_taxonomy_condition_2',
        ],
      ],
    ],
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'entity_visibility_preview',
    'entity_visibility_preview_simple_grants',
    'node',
    'taxonomy',
    'toolbar',
    'views',
  ];

  /**
   * The list of user permissions.
   *
   * @var array
   */
  protected $userPermissions = [
    'access toolbar',
    'entity_visibility_preview_use_preview',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create article node type.
    if ($this->profile != 'standard') {
      $this->drupalCreateContentType([
        'type' => 'article',
        'name' => 'Article',
      ]);
    }

    $this->prepareDates();
    $this->createFieldStorage();
    $this->createFieldConfig();

    foreach ($this->vocabularyIds as $index => $vocabularyId) {
      $taxonomy = Vocabulary::create([
        'name' => $this->randomString(),
        'vid' => $vocabularyId,
      ]);
      $taxonomy->save();

      $selection_handler_settings = [
        'target_bundles' => [
          $vocabularyId => $vocabularyId,
        ],
      ];
      $this->createEntityReferenceField('user', 'user', $this->userFieldNames[$index], $this->randomString(), 'taxonomy_term', 'default', $selection_handler_settings, FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
    }

    $taxonomy_term_1_1 = Term::create([
      'name' => 'Segment 1',
      'vid' => $this->vocabularyIds[0],
      'uuid' => 'segment1_1_uuid',
    ]);
    $taxonomy_term_1_1->save();

    $taxonomy_term_1_2 = Term::create([
      'name' => 'Segment 2',
      'vid' => $this->vocabularyIds[0],
      'uuid' => 'segment1_2_uuid',
    ]);
    $taxonomy_term_1_2->save();

    $taxonomy_term_2_1 = Term::create([
      'name' => 'Subsegment 1',
      'vid' => $this->vocabularyIds[1],
      'uuid' => 'segment2_1_uuid',
    ]);
    $taxonomy_term_2_1->save();

    $taxonomy_term_2_2 = Term::create([
      'name' => 'Subsegment 2',
      'vid' => $this->vocabularyIds[1],
      'uuid' => 'segment2_2_uuid',
    ]);
    $taxonomy_term_2_2->save();

    $nodes = [
      [
        'type' => 'article',
        'title' => 'Article 1 - Segment: 1|Subsegment: 1 - Date: any',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
              $taxonomy_term_2_1->uuid(),
            ],
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 2 - Segment: 2|Subsegment: 1 - Date: any',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_2->uuid(),
              $taxonomy_term_2_1->uuid(),
            ],
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 3 - Segment: 1|Subsegment: 2 - Date: any',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
              $taxonomy_term_2_2->uuid(),
            ],
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 4 - Segment: 2|Subsegment: 2 - Date: any',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_2->uuid(),
              $taxonomy_term_2_2->uuid(),
            ],
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 5 - Segment: 1|Subsegment: 1 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
              $taxonomy_term_2_1->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 6 - Segment: 2|Subsegment: 1 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_2->uuid(),
              $taxonomy_term_2_1->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 7 - Segment: 1|Subsegment: 2 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
              $taxonomy_term_2_2->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 8 - Segment: 2|Subsegment: 2 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_2->uuid(),
              $taxonomy_term_2_2->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 9 - Segment: 1|Subsegment: 1 - Date: +5 days to +15 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
              $taxonomy_term_2_1->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInTheFuture,
            'end_value' => $this->fifteenDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 10 - Segment: 2|Subsegment: 1 - Date: +5 days to +15 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_2->uuid(),
              $taxonomy_term_2_1->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInTheFuture,
            'end_value' => $this->fifteenDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 11 - Segment: 1|Subsegment: 2 - Date: +5 days to +15 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
              $taxonomy_term_2_2->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInTheFuture,
            'end_value' => $this->fifteenDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 12 - Segment: 2|Subsegment: 2 - Date: +5 days to +15 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_2->uuid(),
              $taxonomy_term_2_2->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInTheFuture,
            'end_value' => $this->fifteenDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 13 - Segment: 1 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 14 - Segment: 2 - Date: -5 days to +5 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_2->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInThePast,
            'end_value' => $this->fiveDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 15 - Segment: 1 - Date: +5 days to +15 days',
        'status' => NodeInterface::PUBLISHED,
        $this->fieldName => \serialize([
          'taxonomy_condition' => [
            'taxonomy_term_uuids' => [
              $taxonomy_term_1_1->uuid(),
            ],
          ],
          'date_range_condition' => [
            'value' => $this->fiveDaysInTheFuture,
            'end_value' => $this->fifteenDaysInTheFuture,
          ],
        ]),
      ],
      [
        'type' => 'article',
        'title' => 'Article 16 - No evp',
        'status' => NodeInterface::PUBLISHED,
      ],
    ];

    foreach ($nodes as $node) {
      $this->drupalCreateNode($node);
    }
    \node_access_rebuild();

    $user = $this->drupalCreateUser(
      $this->userPermissions,
      $this->randomString(),
      FALSE,
      [
        'test_taxonomy_condition_1' => [
          [
            'target_id' => $taxonomy_term_1_1->id(),
          ],
        ],
        'test_taxonomy_condition_2' => [
          [
            'target_id' => $taxonomy_term_2_1->id(),
          ],
        ],
      ]
    );
    if (!($user instanceof UserInterface)) {
      $this->fail('Impossible to create the tests user.');
    }
    $this->user = $user;
  }

}
