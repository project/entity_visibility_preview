<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Controller;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns the sessions info.
 */
class PreviewMessage extends ControllerBase {

  /**
   * Instance of EntityVisibilityPreviewConditionPluginManager service.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->entityVisibilityPreviewConditionPluginManager = $container->get('plugin.manager.entity_visibility_preview_condition');
    $instance->renderer = $container->get('renderer');
    return $instance;
  }

  /**
   * Route callback to get sessions infos.
   *
   * Set an entry for each plugin.
   */
  public function getMessage(): CacheableResponseInterface {
    $cache = new CacheableMetadata();
    $cache->addCacheContexts(['session']);

    $response = new CacheableResponse();
    $response->addCacheableDependency($cache);

    $items = [];
    $plugins = $this->entityVisibilityPreviewConditionPluginManager->getDefinitions();
    foreach (\array_keys($plugins) as $plugin_id) {
      /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entityVisibilityPreviewCondition */
      $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($plugin_id);
      $entityVisibilityPreviewCondition->previewMessage($items);
    }

    if (!empty($items)) {
      $build = [
        '#theme' => 'item_list',
        '#title' => $this->t('The website is in preview mode with the further values:'),
        '#items' => $items,
      ];
      $response->setContent($this->renderer->renderRoot($build)->__toString());
    }

    return $response;
  }

}
