<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Intermediate service to help manipulate data in session.
 */
class SessionManager implements SessionManagerInterface {

  /**
   * The session.
   *
   * @var \Symfony\Component\HttpFoundation\Session\SessionInterface|null
   */
  protected $session;

  /**
   * SessionManager service.
   *
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   The session.
   */
  public function __construct(SessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public function getFromSession(string $parameter) {
    if (!empty($this->session)) {
      /** @var array $session */
      $session = $this->session->get(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, []);
      return $session[$parameter] ?? NULL;
    }

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setFromSession(string $parameter, $value): void {
    if (!empty($this->session)) {
      /** @var array $session */
      $session = $this->session->get(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, []);
      $session[$parameter] = $value;
      $this->session->set(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, $session);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteFromSession(string $parameter): void {
    if (!empty($this->session)) {
      /** @var array $session */
      $session = $this->session->get(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, []);
      unset($session[$parameter]);
      $this->session->set(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, $session);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setPreviewIsActive(bool $active): void {
    if (!empty($this->session)) {
      /** @var array $session */
      $session = $this->session->get(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, []);
      $session[self::PREVIEW_ACTIVE_VARIABLE_NAME] = $active;
      $this->session->set(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, $session);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isPreviewActive(): bool {
    if (!empty($this->session)) {
      /** @var array $session */
      $session = $this->session->get(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, []);
      $is_preview_active = $session[self::PREVIEW_ACTIVE_VARIABLE_NAME] ?? FALSE;
      return (bool) $is_preview_active;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSessionData(): array {
    if (!empty($this->session)) {
      return (array) $this->session->get(self::ENTITY_VISIBILITY_PREVIEW_SESSION_KEY, []);
    }
    return [];
  }

}
