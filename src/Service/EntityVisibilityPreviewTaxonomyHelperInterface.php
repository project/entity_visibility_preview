<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Service;

/**
 * Interface for the EntityVisibilityPreviewTaxonomyHelper service.
 */
interface EntityVisibilityPreviewTaxonomyHelperInterface {

  public const LOGGER_CHANNEL = 'entity_visibility_preview_taxonomy_helper';

  /**
   * Returns an array of tids given an array of uuids.
   *
   * @param array $uuids
   *   The uuids to load.
   *
   * @return array
   *   An array of tids.
   */
  public function getTidsFromUuids(array $uuids): array;

  /**
   * Returns an array of uuids given an array of tids.
   *
   * @param array $tids
   *   An array of tids to transform to uuids.
   *
   * @return array
   *   An array of uuids.
   */
  public function getUuidsFromTids(array $tids): array;

  /**
   * Returns an array of terms given an array of uuids.
   *
   * @param array $uuids
   *   The uuids to load.
   *
   * @return \Drupal\taxonomy\Entity\Term[]
   *   An array of terms.
   */
  public function getTermsFromUuids(array $uuids): array;

  /**
   * Returns a vocabulary object given it's ID, or NULL.
   *
   * @param string $vocabularyId
   *   The vocabulary to load.
   *
   * @return \Drupal\taxonomy\Entity\Vocabulary|null
   *   The loaded vocabulary or null.
   */
  public function getVocabularyFromId($vocabularyId);

}
