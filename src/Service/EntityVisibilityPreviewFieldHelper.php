<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Service;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager;

/**
 * Helper to retrieve entity visibility preview fields.
 */
class EntityVisibilityPreviewFieldHelper implements EntityVisibilityPreviewFieldHelperInterface {

  /**
   * The Condition plugin manager.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * An array to store a local cache of entity preview fields.
   *
   * @var array|null
   */
  protected $entityVisibilityPreviewFields;

  /**
   * PreviewForm constructor.
   *
   * @param \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager $manager
   *   The Condition plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(
    EntityVisibilityPreviewConditionPluginManager $manager,
    EntityFieldManagerInterface $entity_field_manager,
    LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    $this->entityVisibilityPreviewConditionPluginManager = $manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->logger = $loggerChannelFactory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public function getAllEntityVisibilityPreviewFields(): array {
    if ($this->entityVisibilityPreviewFields === NULL) {
      try {
        $this->entityVisibilityPreviewFields = [];
        $entity_visibility_preview_fields = $this->entityFieldManager->getFieldMapByFieldType('entity_visibility_preview');
        foreach ($entity_visibility_preview_fields as $entity_type_id => $entity_type_field) {
          foreach ($entity_type_field as $field_machine_name => $field_info) {
            foreach ($field_info['bundles'] as $field_instance_bundle) {
              $field_definition = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $field_instance_bundle)[$field_machine_name];
              /** @var array $field_configuration_enabled_condition */
              $field_configuration_enabled_condition = $field_definition->getSetting('enabled_condition_plugins');
              $enabled_plugin_definitions = $this->entityVisibilityPreviewConditionPluginManager->getEnabledDefinitions($field_configuration_enabled_condition);
              $this->entityVisibilityPreviewFields[$entity_type_id][$field_instance_bundle][$field_definition->getName()] = [
                'field_definition' => $field_definition,
                'enabled_plugins' => $enabled_plugin_definitions,
              ];
            }
          }
        }
      }
      catch (\Exception $e) {
        $this->logger->error('An error occurred retrieving all entity visibility preview fields: :exception', [':exception' => $e->getMessage()]);
      }
    }

    return $this->entityVisibilityPreviewFields;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityVisibilityPreviewFieldsForBundle($entityType, $entityBundle): array {
    $fields = $this->getAllEntityVisibilityPreviewFields();
    foreach ($fields as $fieldEntityType => $fieldInfos) {
      if ($fieldEntityType == $entityType && !empty($fieldInfos[$entityBundle])) {
        return $fieldInfos[$entityBundle];
      }
    }
    return [];
  }

}
