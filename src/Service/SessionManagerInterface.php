<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Service;

/**
 * Interface for the SessionManager service.
 */
interface SessionManagerInterface {

  /**
   * Root session key for all session data.
   */
  public const ENTITY_VISIBILITY_PREVIEW_SESSION_KEY = 'entity_visibility_preview';

  /**
   * Variable used to check if preview is active.
   */
  public const PREVIEW_ACTIVE_VARIABLE_NAME = 'entity_visibility_preview_status';

  /**
   * Get from the actual session the parameter.
   *
   * @param string $parameter
   *   The name of the session parameter to get.
   *
   * @return mixed
   *   The parameter value.
   */
  public function getFromSession(string $parameter);

  /**
   * Set the parameter to the given value.
   *
   * @param string $parameter
   *   The name of the session parameter to set.
   * @param mixed $value
   *   The value to set for the given parameter.
   */
  public function setFromSession(string $parameter, $value): void;

  /**
   * Delete the value of given parameter in the session.
   *
   * @param string $parameter
   *   The name of the session parameter to remove.
   */
  public function deleteFromSession(string $parameter): void;

  /**
   * Sets the preview status to "active / inactive".
   *
   * @param bool $active
   *   The preview status.
   */
  public function setPreviewIsActive(bool $active): void;

  /**
   * Returns TRUE if preview is active, FALSE if not.
   *
   * @return bool
   *   The preview status.
   */
  public function isPreviewActive(): bool;

  /**
   * Returns an array with all data contained in preview session key.
   *
   * @return array
   *   The array of preview settings.
   */
  public function getSessionData(): array;

}
