<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Service;

/**
 * Interface for the EntityVisibilityPreviewFieldHelper service.
 */
interface EntityVisibilityPreviewFieldHelperInterface {

  public const LOGGER_CHANNEL = 'entity_visibility_preview_field_helper';

  /**
   * Returns an array of entity visibility preview fields for all entity types.
   *
   * This array is keyed by entity_type, subkey bundle and subkey is
   * field machine name.
   * Finally, the array contains keys 'field_definition' and 'enabled_plugins'.
   * Example: $fields = [
   *   'node' => [
   *     'article' => [
   *       'field_evp' => [
   *         'field_definition' => ...
   *         'enabled_plugins' => ...
   *       ],
   *     ],
   *   ],
   * ];
   *
   * @return array
   *   An array as explained in upper text.
   */
  public function getAllEntityVisibilityPreviewFields(): array;

  /**
   * Returns array of entity visibility preview fields for given bundle.
   *
   * The array is keyed by field machine names.
   *
   * @param string $entityType
   *   The desired entity type.
   * @param string $entityBundle
   *   The desired bundle.
   *
   * @return array
   *   An array of fields.
   *
   * @see $this->getAllEntityVisibilityPreviewFields()
   */
  public function getEntityVisibilityPreviewFieldsForBundle($entityType, $entityBundle): array;

}
