<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Helper for taxonomies.
 */
class EntityVisibilityPreviewTaxonomyHelper implements EntityVisibilityPreviewTaxonomyHelperInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Static cache for loaded vocabularies.
   *
   * @var array
   */
  protected $vocabularies = [];

  /**
   * PreviewForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger channel factory.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $loggerChannelFactory->get(self::LOGGER_CHANNEL);
  }

  /**
   * {@inheritdoc}
   */
  public function getTidsFromUuids(array $uuids): array {
    try {
      /** @var array $resultTids */
      $resultTids = $this->entityTypeManager->getStorage('taxonomy_term')
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('uuid', $uuids, 'IN')
        ->execute();
    }
    catch (\Exception $e) {
      $resultTids = [];
    }

    return $resultTids;
  }

  /**
   * {@inheritdoc}
   */
  public function getUuidsFromTids(array $tids): array {
    try {
      $uuids = [];
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadMultiple($tids);

      foreach ($terms as $term) {
        $uuids[] = $term->uuid();
      }
      return $uuids;
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTermsFromUuids(array $uuids): array {
    try {
      $ids = $this->getTidsFromUuids($uuids);
      /** @var array $resultTerms */
      $resultTerms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($ids);
    }
    catch (\Exception $e) {
      $resultTerms = [];
    }

    return $resultTerms;
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyFromId($vocabularyId) {
    try {
      if (empty($this->vocabularies[$vocabularyId])) {
        $this->vocabularies[$vocabularyId] = $this->entityTypeManager->getStorage('taxonomy_vocabulary')
          ->load($vocabularyId);
      }
    }
    catch (\Exception $e) {
      return NULL;
    }

    return $this->vocabularies[$vocabularyId];
  }

}
