<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages EntityVisibilityPreview plugins.
 */
class EntityVisibilityPreviewConditionPluginManager extends DefaultPluginManager {

  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/EntityVisibilityPreviewCondition';
    $plugin_interface = 'Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface';
    $plugin_definition_annotation_name = 'Drupal\entity_visibility_preview\Annotation\EntityVisibilityPreviewCondition';
    $plugin_definition_attribute_name = 'Drupal\entity_visibility_preview\Attribute\EntityVisibilityPreviewCondition';
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_attribute_name, $plugin_definition_annotation_name);
    $this->alterInfo('entity_visibility_preview_info');
    $this->setCacheBackend($cache_backend, 'entity_visibility_preview_plugins');
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  protected function alterDefinitions(&$definitions): void {
    // Loop through all definitions.
    foreach ($definitions as $definition_key => $definition_info) {
      // Check to see if dependencies key is set.
      if (!empty($definition_info['dependencies'])) {
        // Loop through dependencies to confirm if enabled.
        foreach ($definition_info['dependencies'] as $dependency) {
          // If dependency is not enabled removed from list of definitions.
          if (!$this->moduleHandler->moduleExists($dependency)) {
            unset($definitions[$definition_key]);
            continue;
          }
        }
      }
    }

    parent::alterDefinitions($definitions);
  }

  /**
   * Get only the list of enabled plugin IDs.
   *
   * @param array $configuration
   *   The list of enabled plugin IDs.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @return array
   *   The list of enabled plugin definitions.
   */
  public function getEnabledDefinitions(array $configuration): array {
    $enabled_plugins = [];

    foreach ($this->getDefinitions() as $plugin_id => $plugin_definition) {
      if (isset($configuration[$plugin_id]) && !empty($configuration[$plugin_id])) {
        $enabled_plugins[$plugin_id] = $plugin_definition;
      }
    }

    return $enabled_plugins;
  }

}
