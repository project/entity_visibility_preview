<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a EntityVisibilityPreviewCondition attribute for plugin discovery.
 *
 * @ingroup plugin_api
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class EntityVisibilityPreviewCondition extends Plugin {

  /**
   * Constructs an EntityVisibilityPreviewCondition attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   (optional) The human-readable name of the Entity Visibility Preview
   *   Condition.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the Entity Visibility Preview
   *   Condition.
   * @param string[] $dependencies
   *   (optional) The dependencies to inject.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label = NULL,
    public readonly ?TranslatableMarkup $description = NULL,
    public readonly array $dependencies = [],
  ) {}

}
