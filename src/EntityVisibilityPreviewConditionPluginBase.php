<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\entity_visibility_preview\Service\SessionManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for EntityVisibilityPreviewCondition plugins.
 */
abstract class EntityVisibilityPreviewConditionPluginBase extends PluginBase implements EntityVisibilityPreviewConditionInterface, ContainerFactoryPluginInterface {

  /**
   * The entity visibility preview session service.
   *
   * @var \Drupal\entity_visibility_preview\Service\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin definition.
   * @param \Drupal\entity_visibility_preview\Service\SessionManagerInterface $session_manager
   *   The entity visibility preview session service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    SessionManagerInterface $session_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->sessionManager = $session_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_visibility_preview.session_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $values): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(array $values): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function accessCheck(array $values, FieldDefinitionInterface $field_definition): AccessResultInterface {
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  public function previewBuildForm(array &$form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function previewSubmitForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * {@inheritdoc}
   */
  public function previewResetForm(): void {}

  /**
   * {@inheritdoc}
   */
  public function previewMessage(array &$items): void {}

  /**
   * {@inheritdoc}
   */
  public function conditionDisplay($condition): array {
    return [];
  }

}
