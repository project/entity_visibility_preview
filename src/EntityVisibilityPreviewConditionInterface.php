<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * An interface for all EntityVisibilityPreviewCondition plugins.
 */
interface EntityVisibilityPreviewConditionInterface {

  /**
   * Builds the form element for a Entity Visibility Preview Condition field.
   *
   * @param array $values
   *   Existing values.
   *
   * @return array
   *   Render array for EntityVisibilityPreviewCondition form.
   */
  public function form(array $values): array;

  /**
   * Validation and process of the Entity Visibility Preview Condition field.
   *
   * @param array $values
   *   An array containing values of the condition plugin.
   *
   * @return array
   *   The values to store for the plugin.
   */
  public function formSubmit(array $values): array;

  /**
   * Builds the form element for plugin configuration on the field level.
   *
   * @return array
   *   Plugin form array.
   */
  public function fieldSettingsForm(): array;

  /**
   * Custom validation of the Entity Visibility Preview Condition field.
   *
   * @param array $values
   *   An array containing values of the condition plugin.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   Access result.
   */
  public function accessCheck(array $values, FieldDefinitionInterface $field_definition): AccessResultInterface;

  /**
   * Preview form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form with the new plugin preview part.
   */
  public function previewBuildForm(array &$form, FormStateInterface $form_state): array;

  /**
   * Preview form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function previewSubmitForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Preview form reset handler.
   */
  public function previewResetForm(): void;

  /**
   * Preview message when the session have plugin value.
   *
   * @param array $items
   *   The item list to display in the message.
   */
  public function previewMessage(array &$items): void;

  /**
   * Returns a renderable array according to condition value.
   *
   * @param mixed $condition
   *   The plugin condition value.
   *
   * @return array
   *   A renderable array.
   */
  public function conditionDisplay($condition): array;

}
