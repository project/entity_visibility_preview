<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\HookHandler;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hook handler for the toolbar() hook.
 */
class ToolbarHookHandler implements ContainerInjectionInterface, TrustedCallbackInterface {

  use StringTranslationTrait;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  private $redirectDestination;

  /**
   * ToolbarHookHandler constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    RedirectDestinationInterface $redirect_destination,
  ) {
    $this->currentUser = $current_user;
    $this->redirectDestination = $redirect_destination;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('current_user'),
      $container->get('redirect.destination')
    );
  }

  /**
   * Add a custom toolbar element to the preview form in a modal.
   *
   * @return array
   *   The new toolbar item.
   */
  public function process(): array {
    $items = [
      'entity_visibility_preview' => [
        '#cache' => [
          'contexts' => ['user.permissions'],
        ],
      ],
    ];
    $user_access = $this->currentUser->hasPermission('entity_visibility_preview_use_preview');
    if ($user_access) {
      $items['entity_visibility_preview'] += [
        '#type' => 'toolbar_item',
        'tab' => [
          '#type' => 'link',
          '#title' => $this->t('Website preview'),
          '#url' => Url::fromRoute('entity_visibility_preview.preview_form'),
          '#attributes' => [
            'title' => $this->t('Website preview'),
            'class' => [
              'toolbar-icon',
              'toolbar-icon-entity-visibility-preview',
            ],
          ],
        ],
        'tray' => [
          '#heading' => $this->t('Website preview'),
          'entity_visibility_preview_form' => [
            // @phpstan-ignore-next-line
            '#lazy_builder' => [__CLASS__ . ':lazyBuilder', []],
            '#create_placeholder' => TRUE,
          ],
        ],
        '#wrapper_attributes' => [
          'class' => ['entity-visibility-preview-toolbar-tab'],
        ],
        '#attached' => [
          'library' => [
            'entity_visibility_preview/toolbar',
          ],
        ],
      ];
    }

    return $items;
  }

  /**
   * Lazy builder callback for the entity visibility preview menu toolbar.
   *
   * @return array
   *   The renderable array representation.
   */
  public function lazyBuilder(): array {
    return [
      'entity_visibility_preview_form' => [
        '#type' => 'link',
        '#title' => $this->t('Website preview form'),
        '#url' => Url::fromRoute('entity_visibility_preview.preview_form', [], [
          'query' => $this->redirectDestination->getAsArray(),
        ]),
        '#attributes' => [
          'title' => $this->t('Website preview form'),
          'class' => [
            'use-ajax',
          ],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => \json_encode([
            'width' => '50%',
          ]),
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['lazyBuilder'];
  }

}
