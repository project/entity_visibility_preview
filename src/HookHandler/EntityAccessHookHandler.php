<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\HookHandler;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_visibility_preview\Cache\Context\PreviewIsActiveCacheContextIdentifier;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager;
use Drupal\entity_visibility_preview\Service\SessionManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hook handler for the entity_access() hook.
 */
class EntityAccessHookHandler implements ContainerInjectionInterface {

  /**
   * The entity visibility preview condition plugin manager.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * The admin context.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Preview session manager.
   *
   * @var \Drupal\entity_visibility_preview\Service\SessionManagerInterface
   */
  protected $previewSessionManager;

  /**
   * EntityAccessHookHandler constructor.
   *
   * @param \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager $entity_visibility_preview_condition_plugin_manager
   *   The entity visibility preview condition plugin manager.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context.
   * @param \Drupal\entity_visibility_preview\Service\SessionManagerInterface $previewSessionManager
   *   Preview session manager.
   */
  public function __construct(
    EntityVisibilityPreviewConditionPluginManager $entity_visibility_preview_condition_plugin_manager,
    AdminContext $admin_context,
    SessionManagerInterface $previewSessionManager,
  ) {
    $this->entityVisibilityPreviewConditionPluginManager = $entity_visibility_preview_condition_plugin_manager;
    $this->adminContext = $admin_context;
    $this->previewSessionManager = $previewSessionManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.entity_visibility_preview_condition'),
      $container->get('router.admin_context'),
      $container->get('entity_visibility_preview.session_manager')
    );
  }

  /**
   * Apply the various visibility conditions on the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check.
   * @param string $operation
   *   The actual operation.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user who access the entity.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   TRUE if the condition is satisfied, FALSE otherwise.
   */
  public function process(EntityInterface $entity, string $operation, AccountInterface $account): AccessResultInterface {
    if ($operation != 'view') {
      return AccessResult::neutral();
    }
    if (!($entity instanceof ContentEntityInterface)) {
      return AccessResult::neutral();
    }
    // Don't interfere with the access checks in back office.
    if ($this->adminContext->isAdminRoute()) {
      return AccessResult::neutral();
    }
    if (!$this->previewSessionManager->isPreviewActive() && $account->hasPermission('entity_visibility_preview_bypass_access')) {
      return AccessResult::neutral()->addCacheContexts([PreviewIsActiveCacheContextIdentifier::CONTEXT_ID]);
    }

    $access_results = [];
    $fields = $entity->getFields();
    foreach ($fields as $field) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
      $field_definition = $field->getFieldDefinition();
      if ($field_definition->getType() == 'entity_visibility_preview' && !$field->isEmpty()) {
        /** @var array $enabled_condition_plugins */
        $enabled_condition_plugins = $field_definition->getSetting('enabled_condition_plugins');
        /** @var array $condition_plugins_settings */
        $condition_plugins_settings = $field_definition->getSetting('condition_plugins_settings');
        /** @var array $field_values */
        $field_values = $field->getValue();
        /** @var array $entity_conditions */
        $entity_conditions = \unserialize($field_values[0]['value'], [
          'allowed_classes' => FALSE,
        ]);
        foreach ($entity_conditions as $condition_plugin_id => $condition_value) {
          // Check if the condition plugin is enabled in the field settings and
          // if we have configuration.
          if (
            \in_array($condition_plugin_id, $enabled_condition_plugins, TRUE)
            && !empty($enabled_condition_plugins[$condition_plugin_id])
            && $this->entityVisibilityPreviewConditionPluginManager->hasDefinition($condition_plugin_id)
          ) {
            /** @var array $configuration */
            $configuration = $condition_plugins_settings[$condition_plugin_id] ?? [];
            /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entityVisibilityPreviewCondition */
            $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($condition_plugin_id, $configuration);
            $access_results[] = $entityVisibilityPreviewCondition->accessCheck($condition_value, $field_definition);
          }
        }
      }
    }

    // Merge cache metadata and result Forbidden if one plugin return forbidden.
    $result = AccessResult::neutral();
    $result_cache = new CacheableMetadata();
    foreach ($access_results as $access_result) {
      $result_cache->addCacheableDependency($access_result);
      if ($access_result->isForbidden()) {
        $result = $access_result;
      }
    }
    // @phpstan-ignore-next-line
    $result->addCacheableDependency($result_cache);
    // @phpstan-ignore-next-line
    $result->addCacheContexts([PreviewIsActiveCacheContextIdentifier::CONTEXT_ID]);

    return $result;
  }

}
