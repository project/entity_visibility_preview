<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\HookHandler;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Hook handler for the preprocess_page() hook.
 */
class PreprocessPageHookHandler implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * PreprocessPageHookHandler constructor.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(
    AccountProxyInterface $current_user,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->currentUser = $current_user;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('current_user'),
      $container->get('config.factory')
    );
  }

  /**
   * Add JS to display message when preview functionality is used.
   *
   * @param array $variables
   *   The page variables.
   */
  public function process(array &$variables): void {
    $config = $this->configFactory->get('entity_visibility_preview.settings');

    // The cache should be invalidated when this config is changed.
    $cache = new CacheableMetadata();
    $cache->addCacheableDependency($config);
    if ($config->get('display_preview_message')) {
      $cache->addCacheContexts(['user.permissions']);
      if ($this->currentUser->hasPermission('entity_visibility_preview_use_preview')) {
        $library_info = [
          '#attached' => [
            'library' => [
              'entity_visibility_preview/preview_message',
            ],
          ],
        ];
        $variables = \array_merge_recursive($variables, $library_info);
      }
    }
    $cache->applyTo($variables);
  }

}
