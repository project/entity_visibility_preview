<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\entity_visibility_preview\Service\SessionManagerInterface;

/**
 * Defines a cache context defined by entity_visibility_preview status.
 *
 * Cache context ID: 'entity_visibility_preview_preview_settings'.
 */
class PreviewIsActiveCacheContextIdentifier implements CacheContextInterface {

  /**
   * Context ID.
   *
   * @const string
   */
  public const CONTEXT_ID = 'entity_visibility_preview_preview_settings';

  /**
   * Preview session manager.
   *
   * @var \Drupal\entity_visibility_preview\Service\SessionManagerInterface
   */
  protected $previewSessionManager;

  /**
   * PreviewIsActiveCacheContextIdentifier constructor.
   *
   * @param \Drupal\entity_visibility_preview\Service\SessionManagerInterface $previewSessionManager
   *   Preview session manager.
   */
  public function __construct(
    SessionManagerInterface $previewSessionManager,
  ) {
    $this->previewSessionManager = $previewSessionManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return \t('Cache context that returns entity visibility preview settings hash.');
  }

  /**
   * Returns a hash of preview settings.
   *
   * {@inheritdoc}
   */
  public function getContext() {
    return \md5(\serialize($this->previewSessionManager->getSessionData()));
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
