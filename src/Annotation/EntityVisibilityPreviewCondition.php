<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object.
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class EntityVisibilityPreviewCondition extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label.
   *
   * @var string
   */
  public $label;

  /**
   * The plugin description.
   *
   * @var string
   */
  public $description;

  /**
   * The name of modules that are required for this condition to be usable.
   *
   * @var array
   */
  public $dependencies;

}
