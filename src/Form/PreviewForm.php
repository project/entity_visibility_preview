<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager;
use Drupal\entity_visibility_preview\Service\SessionManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Preview form.
 */
class PreviewForm extends FormBase {

  /**
   * Instance of EntityVisibilityPreviewConditionPluginManager service.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * Preview session manager.
   *
   * @var \Drupal\entity_visibility_preview\Service\SessionManagerInterface
   */
  protected $previewSessionManager;

  /**
   * PreviewForm constructor.
   *
   * @param \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager $manager
   *   The Condition plugin manager.
   * @param \Drupal\entity_visibility_preview\Service\SessionManagerInterface $previewSessionManager
   *   Preview session manager service.
   */
  public function __construct(
    EntityVisibilityPreviewConditionPluginManager $manager,
    SessionManagerInterface $previewSessionManager,
  ) {
    $this->entityVisibilityPreviewConditionPluginManager = $manager;
    $this->previewSessionManager = $previewSessionManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.entity_visibility_preview_condition'),
      $container->get('entity_visibility_preview.session_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_visibility_preview_preview_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $plugins = $this->entityVisibilityPreviewConditionPluginManager->getDefinitions();
    foreach (\array_keys($plugins) as $plugin_id) {
      /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entityVisibilityPreviewCondition */
      $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($plugin_id);
      $entityVisibilityPreviewCondition->previewBuildForm($form, $form_state);
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];
    $form['actions']['reset'] = [
      '#type' => 'submit',
      '#submit' => ['::resetForm'],
      '#value' => $this->t('Reset to current date and normal state'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $plugins = $this->entityVisibilityPreviewConditionPluginManager->getDefinitions();
    foreach (\array_keys($plugins) as $plugin_id) {
      /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entityVisibilityPreviewCondition */
      $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($plugin_id);
      $entityVisibilityPreviewCondition->previewSubmitForm($form, $form_state);
    }
    $this->previewSessionManager->setPreviewIsActive(TRUE);
  }

  /**
   * Submit callback for the reset button.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function resetForm(array &$form, FormStateInterface $form_state): void {
    $plugins = $this->entityVisibilityPreviewConditionPluginManager->getDefinitions();
    foreach (\array_keys($plugins) as $plugin_id) {
      /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entityVisibilityPreviewCondition */
      $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($plugin_id);
      $entityVisibilityPreviewCondition->previewResetForm();
    }
    $this->previewSessionManager->setPreviewIsActive(FALSE);
  }

}
