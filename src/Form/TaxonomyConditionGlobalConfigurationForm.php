<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\State\StateInterface;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Global configuration for Entity visibility preview taxonomy condition.
 */
class TaxonomyConditionGlobalConfigurationForm extends ConfigFormBase {

  /**
   * Length of the prefix 'anonymous_term__'.
   */
  public const ENTITY_AUTOCOMPLETE_FORM_ELEMENT_PREFIX_LENGTH = 16;

  /**
   * Key of the entity type ID part when parsing the form element.
   */
  public const ENTITY_AUTOCOMPLETE_FORM_ELEMENT_ENTITY_TYPE_PARSING_KEY = 1;

  /**
   * Key of the bundle part when parsing the form element.
   */
  public const ENTITY_AUTOCOMPLETE_FORM_ELEMENT_BUNDLE_PARSING_KEY = 2;

  /**
   * Key of the field machine name part when parsing the form element.
   */
  public const ENTITY_AUTOCOMPLETE_FORM_ELEMENT_FIELD_MACHINE_NAME_PARSING_KEY = 3;

  /**
   * The Condition plugin manager.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The bundle infos.
   *
   * @var array
   */
  protected $bundleInfos;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * PreviewForm constructor.
   *
   * @param \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager $manager
   *   The Condition plugin manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info service.
   * @param \Drupal\Core\State\StateInterface $state_service
   *   The state service.
   */
  public function __construct(
    EntityVisibilityPreviewConditionPluginManager $manager,
    EntityFieldManagerInterface $entity_field_manager,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    StateInterface $state_service,
  ) {
    $this->entityVisibilityPreviewConditionPluginManager = $manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->bundleInfos = $entity_type_bundle_info->getAllBundleInfo();
    $this->state = $state_service;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.entity_visibility_preview_condition'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('state')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_visibility_preview_taxonomy_condition_global_configuration';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_visibility_preview.taxonomy_condition_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_visibility_preview_taxonomy_condition_settings = $this->config('entity_visibility_preview.taxonomy_condition_settings');
    $form['taxonomy_selection_widget'] = [
      '#type' => 'select',
      '#title' => $this->t('Widget used to select taxonomy terms for EVP'),
      '#description' => $this->t('This setting applies on entity forms with EVP fields and on EVP preview form.'),
      '#options' => [
        'autocomplete' => $this->t('Autocomplete'),
        'checkboxes' => $this->t('Checkboxes'),
      ],
      '#default_value' => $entity_visibility_preview_taxonomy_condition_settings->get('taxonomy_selection_widget'),
    ];

    $form['preview_taxonomy_vocabularies'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Vocabularies used for preview form'),
      '#description' => $this->t('Terms in these vocabularies will be proposed on EVP preview form.'),
      '#options' => $this->getVocabularyBundles(),
      '#default_value' => $entity_visibility_preview_taxonomy_condition_settings->get('preview_taxonomy_vocabularies'),
    ];

    $entity_type_definitions = $this->entityTypeManager->getDefinitions();
    $entity_visibility_preview_fields = $this->entityFieldManager->getFieldMapByFieldType('entity_visibility_preview');
    foreach ($entity_visibility_preview_fields as $entity_type_id => $entity_type_field) {
      foreach ($entity_type_field as $field_machine_name => $field_info) {
        foreach ($field_info['bundles'] as $field_instance_bundle) {
          $field_definition = $this->entityFieldManager->getFieldDefinitions($entity_type_id, $field_instance_bundle)[$field_machine_name];
          /** @var array $field_enabled_condition_plugins */
          $field_enabled_condition_plugins = $field_definition->getSetting('enabled_condition_plugins');
          $enabled_plugin_definitions = $this->entityVisibilityPreviewConditionPluginManager->getEnabledDefinitions($field_enabled_condition_plugins);
          foreach (\array_keys($enabled_plugin_definitions) as $plugin_id) {
            if ($plugin_id == 'taxonomy_condition') {
              /** @var array $condition_plugin_settings */
              $condition_plugin_settings = $field_definition->getSetting('condition_plugins_settings');

              $state_key_elements = [
                'entity_visibility_preview',
                'anonymous',
                'taxonomy_condition',
                $entity_type_id,
                $field_instance_bundle,
                $field_machine_name,
              ];

              /** @var array|null $state */
              $state = $this->state->get(\implode(':', $state_key_elements), []);
              $default_value = [];
              if (!empty($state)) {
                $default_value = $this->entityTypeManager->getStorage('taxonomy_term')
                  ->loadMultiple($state);
              }

              $form['anonymous_term__' . $entity_type_id . '__' . $field_instance_bundle . '__' . $field_machine_name] = [
                '#type' => 'entity_autocomplete',
                '#title' => $this->t('Anonymous categorization for @entity_type / @bundle / @field.', [
                  '@entity_type' => $entity_type_definitions[$entity_type_id]->getLabel(),
                  '@bundle' => $this->bundleInfos[$entity_type_id][$field_instance_bundle]['label'],
                  '@field' => $field_definition->getLabel(),
                ]),
                '#description' => $this->t('Enter the taxonomy terms that the anonymous user will have when checking access to content using this field. Enter a comma-separated list.'),
                '#target_type' => 'taxonomy_term',
                '#selection_settings' => [
                  'target_bundles' => $condition_plugin_settings[$plugin_id]['bundles'],
                ],
                '#tags' => TRUE,
                '#default_value' => $default_value,
              ];

              break;
            }
          }
        }
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValues();

    foreach ($values as $key => $value) {
      if (\substr($key, 0, self::ENTITY_AUTOCOMPLETE_FORM_ELEMENT_PREFIX_LENGTH) === 'anonymous_term__') {
        $parsed_key = \explode('__', $key);
        $entity_type_id = $parsed_key[self::ENTITY_AUTOCOMPLETE_FORM_ELEMENT_ENTITY_TYPE_PARSING_KEY];
        $bundle_id = $parsed_key[self::ENTITY_AUTOCOMPLETE_FORM_ELEMENT_BUNDLE_PARSING_KEY];
        $field_machine_name = $parsed_key[self::ENTITY_AUTOCOMPLETE_FORM_ELEMENT_FIELD_MACHINE_NAME_PARSING_KEY];

        $state_key_elements = [
          'entity_visibility_preview',
          'anonymous',
          'taxonomy_condition',
          $entity_type_id,
          $bundle_id,
          $field_machine_name,
        ];

        $state_value = [];
        if (\is_array($value)) {
          foreach ($value as $value_structure) {
            $state_value[] = $value_structure['target_id'];
          }
        }

        $this->state->set(\implode(':', $state_key_elements), $state_value);
      }
    }

    $this->config('entity_visibility_preview.taxonomy_condition_settings')
      ->set('taxonomy_selection_widget', $form_state->getValue('taxonomy_selection_widget'))
      ->set('preview_taxonomy_vocabularies', $form_state->getValue('preview_taxonomy_vocabularies'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * The taxonomy terms bundle list to use in checkboxes options.
   *
   * @return array
   *   The taxonomy terms bundle list.
   */
  protected function getVocabularyBundles(): array {
    $bundle_options = [];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('taxonomy_term');
    foreach ($bundles as $bundle_id => $bundle) {
      $bundle_options[$bundle_id] = $bundle['label'];
    }

    return $bundle_options;
  }

}
