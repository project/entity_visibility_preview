<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Global settings for Entity Visibility Preview.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_visibility_preview_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_visibility_preview.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_visibility_preview_settings = $this->config('entity_visibility_preview.settings');
    $form['display_preview_message'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display preview message'),
      '#description' => $this->t('When the preview form is used, whether to display a message with the preview data or not.'),
      '#default_value' => $entity_visibility_preview_settings->get('display_preview_message'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('entity_visibility_preview.settings')
      ->set('display_preview_message', $form_state->getValue('display_preview_message'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
