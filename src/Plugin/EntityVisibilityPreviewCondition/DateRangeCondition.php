<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\EntityVisibilityPreviewCondition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\entity_visibility_preview\Attribute\EntityVisibilityPreviewCondition;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows to set a date range condition.
 */
#[EntityVisibilityPreviewCondition(
  id: 'date_range_condition',
  label: new TranslatableMarkup('Date range condition'),
  dependencies: ['datetime'],
)]
class DateRangeCondition extends EntityVisibilityPreviewConditionPluginBase {

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The value of the start display date in this instance.
   *
   * @var string
   */
  protected $value;

  /**
   * The value of the end display date in this instance.
   *
   * @var string
   */
  protected $endValue;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->time = $container->get('datetime.time');
    return $instance;
  }

  /**
   * Obtain the current start display date raw value.
   *
   * @return string
   *   The current raw start display date value.
   */
  public function value(): string {
    return $this->value;
  }

  /**
   * Assign the current start display date a value.
   *
   * @param string $value
   *   The value to assign this start display date.
   */
  public function setValue(string $value): void {
    $this->value = $value;
  }

  /**
   * Obtain the current end display date raw value.
   *
   * @return string
   *   The current raw end display date value.
   */
  public function endValue(): string {
    return $this->endValue;
  }

  /**
   * Assign the current end display date a value.
   *
   * @param string $endValue
   *   The value to assign this end display date.
   */
  public function setEndValue(string $endValue): void {
    $this->endValue = $endValue;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $values): array {
    if (isset($values['value'])) {
      $this->setValue($values['value']);
    }
    if (isset($values['end_value'])) {
      $this->setEndValue($values['end_value']);
    }

    return [
      'value' => [
        '#type' => 'date',
        '#title' => $this->t('Start of the display'),
        '#default_value' => $this->value,
      ],
      'end_value' => [
        '#type' => 'date',
        '#title' => $this->t('End of the display'),
        '#description' => $this->t('The content will not be accessible the day of the end date.'),
        '#default_value' => $this->endValue,
      ],
      '#element_validate' => [
        [$this, 'validateStartEnd'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(array $values): array {
    if (isset($values['value']) || isset($values['end_value'])) {
      if ((isset($values['value'], $values['end_value'])) && ($values['end_value'] >= $values['value'])) {
        return $values;
      }
      return $values;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function accessCheck(array $values, FieldDefinitionInterface $field_definition): AccessResultInterface {
    // Get the actual date with the good format.
    $actual_date_timestamp = $this->getActualTimestamp();
    // A cache max age on end of the day to rebuild the cache of entity who use
    // the Date range condition to have the access result re-evaluated each day.
    $actual_date_clearing_cache = DrupalDateTime::createFromTimestamp($actual_date_timestamp)
      ->setTime((int) 23, (int) 59, (int) 59);
    $cache_max_time = $actual_date_clearing_cache->getTimestamp() - $actual_date_timestamp;
    $cache_contexts = [
      'timezone',
      'session',
    ];

    $access_result = AccessResult::forbidden();
    // Date interval.
    if (!empty($values['value']) && !empty($values['end_value'])) {
      $start_display_timestamp = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $values['value'])
        ->setTime(0, 0, 0)
        ->getTimestamp();
      $end_display_timestamp = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $values['end_value'])
        ->setTime(0, 0, 0)
        ->getTimestamp();
      if (($start_display_timestamp <= $actual_date_timestamp) && ($actual_date_timestamp < $end_display_timestamp)) {
        $access_result = AccessResult::neutral();
      }
    }
    // Only start date.
    elseif (!empty($values['value'])) {
      $start_display_timestamp = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $values['value'])
        ->setTime(0, 0, 0)
        ->getTimestamp();
      if ($start_display_timestamp <= $actual_date_timestamp) {
        $access_result = AccessResult::neutral();
      }
    }
    // Only end date.
    elseif (!empty($values['end_value'])) {
      $end_display_timestamp = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $values['end_value'])
        ->setTime(0, 0, 0)
        ->getTimestamp();
      if ($actual_date_timestamp < $end_display_timestamp) {
        $access_result = AccessResult::neutral();
      }
    }
    // No value.
    elseif (empty($values['value']) && empty($values['end_value'])) {
      $access_result = AccessResult::neutral();
    }

    return $access_result
      ->addCacheContexts($cache_contexts)
      ->setCacheMaxAge($cache_max_time);
  }

  /**
   * Get the actual date depending if visitor usage or a preview.
   *
   * @return int
   *   The current timestamp or the preview timestamp for the current user.
   */
  public function getActualTimestamp(): int {
    $actual_date_timestamp = $this->time->getRequestTime();
    /** @var string|null $preview_date_display */
    $preview_date_display = $this->sessionManager->getFromSession('entity_visibility_preview_date_display');

    if ($preview_date_display !== NULL) {
      $actual_date_timestamp = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $preview_date_display)
        ->setTime(0, 0, 0)
        ->getTimestamp();
    }

    return $actual_date_timestamp;
  }

  /**
   * {@inheritdoc}
   */
  public function previewBuildForm(array &$form, FormStateInterface $form_state): array {
    $preview_date_display = $this->sessionManager->getFromSession('entity_visibility_preview_date_display');
    if ($preview_date_display !== NULL) {
      $actual_date = $preview_date_display;
    }
    else {
      $current_date = new DrupalDateTime();
      $actual_date = $current_date->format('Y-m-d');
    }

    $form['preview_date_display'] = [
      '#type' => 'date',
      '#title' => $this->t('Display date'),
      '#description' => $this->t('Choose the preview date to display for the website.'),
      '#default_value' => $actual_date,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function previewSubmitForm(array &$form, FormStateInterface $form_state): void {
    // Set the chosen display date in the session.
    $preview_value = $form_state->getValue(['preview_date_display']);
    if (!empty($preview_value)) {
      $this->sessionManager->setFromSession('entity_visibility_preview_date_display', $preview_value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function previewResetForm(): void {
    $this->sessionManager->deleteFromSession('entity_visibility_preview_date_display');
  }

  /**
   * {@inheritdoc}
   */
  public function previewMessage(array &$items): void {
    $preview_date_display = $this->sessionManager->getFromSession('entity_visibility_preview_date_display');
    if ($preview_date_display !== NULL) {
      $items[] = $this->t('Display date: @date', [
        '@date' => $preview_date_display,
      ]);
    }
  }

  /**
   * Element validate callback to ensure that the start date <= the end date.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   */
  public function validateStartEnd(array &$element, FormStateInterface $form_state, array &$complete_form): void {
    $start_date = $element['value']['#value'];
    $end_date = $element['end_value']['#value'];

    if (!empty($start_date) && !empty($end_date)) {
      $start_timestamp = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $start_date)
        ->getTimestamp();
      $end_timestamp = DrupalDateTime::createFromFormat(DateTimeItemInterface::DATE_STORAGE_FORMAT, $end_date)
        ->getTimestamp();

      if ($end_timestamp < $start_timestamp) {
        $form_state->setError($element, $this->t('The condition end date cannot be before the start date'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function conditionDisplay($condition): array {
    $return = [];
    $text = '';
    /** @var array $condition */
    if (!empty($condition['value']) && !empty($condition['end_value'])) {
      $text = $this->t('Accessible from @start_date until @end_date.', [
        '@start_date' => $condition['value'],
        '@end_date' => $condition['end_value'],
      ]);
    }
    elseif (!empty($condition['value'])) {
      $text = $this->t('Accessible from @start_date.', [
        '@start_date' => $condition['value'],
      ]);
    }
    elseif (!empty($condition['end_value'])) {
      $text = $this->t('Accessible until @end_date.', [
        '@end_date' => $condition['end_value'],
      ]);
    }

    if (!empty($text)) {
      $return = [
        '#type' => 'html_tag',
        '#tag' => 'strong',
        '#value' => $text,
      ];
    }
    return $return;
  }

}
