<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\EntityVisibilityPreviewCondition;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\entity_visibility_preview\Attribute\EntityVisibilityPreviewCondition;
use Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Allows to set a taxonomy term condition.
 */
#[EntityVisibilityPreviewCondition(
  id: 'taxonomy_condition',
  label: new TranslatableMarkup('Taxonomy condition'),
  dependencies: ['taxonomy'],
)]
class TaxonomyCondition extends EntityVisibilityPreviewConditionPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Instance of EntityFieldManagerInterface manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  private $entityFieldManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Taxonomy helper.
   *
   * @var \Drupal\entity_visibility_preview\Service\EntityVisibilityPreviewTaxonomyHelperInterface
   */
  protected $taxonomyHelper;

  /**
   * Taxonomy condition config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $taxonomyConditionConfig;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->state = $container->get('state');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->taxonomyHelper = $container->get('entity_visibility_preview.taxonomy_helper');
    $instance->taxonomyConditionConfig = $container->get('config.factory')->get('entity_visibility_preview.taxonomy_condition_settings');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(): array {
    $form = [];

    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select one or more taxonomy vocabularies.'),
      '#options' => $this->getBundlesOptions(),
      '#default_value' => $this->configuration['bundles'] ?? [],
    ];

    $url = Url::fromRoute('entity_visibility_preview.taxonomy_condition_global_configuration_form');
    if ($url->access()) {
      $form['bundles']['#description'] = $this->t('Configure the terms for the anonymous user on <a href=":url">the taxonomy condition settings page</a>.', [':url' => $url->toString()]);
    }

    $form['user_fields'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t("Select one or more user's taxonomy reference fields to compare with for the access."),
      '#options' => $this->getUserFieldsOptions(),
      '#default_value' => $this->configuration['user_fields'] ?? [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $values): array {
    $default_value = [];
    $tids = [];
    if (isset($values['taxonomy_term_uuids'])) {
      $tids = $this->taxonomyHelper->getTidsFromUuids($values['taxonomy_term_uuids']);
      $default_value = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadMultiple($tids);
    }

    $form = [];
    switch ($this->taxonomyConditionConfig->get('taxonomy_selection_widget')) {
      case 'checkboxes':
        $bundles = [];
        foreach ($this->configuration['bundles'] as $vid => $enabled) {
          if ($enabled != '0') {
            $bundles[] = $vid;
          }
        }
        $form = $this->getTaxonomyForm($bundles, $tids);
        break;

      default:
        $form['taxonomy_term_tids'] = [
          '#type' => 'entity_autocomplete',
          '#title' => $this->t('Categorization'),
          '#description' => $this->t('Enter at least one taxonomy term that the user must have to access to this content. If you enter multiple terms, only one is enough to access this content. Enter a comma-separated list.'),
          '#target_type' => 'taxonomy_term',
          '#selection_settings' => [
            'target_bundles' => $this->configuration['bundles'],
          ],
          '#tags' => TRUE,
          '#default_value' => $default_value,
        ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function formSubmit(array $values): array {
    $taxonomy_terms = [];

    if ($this->taxonomyConditionConfig->get('taxonomy_selection_widget') == 'checkboxes') {
      $taxonomy_terms['taxonomy_term_uuids'] = $this->getUuidsFromCheckboxes($values);
    }
    else {
      if (isset($values['taxonomy_term_tids'])) {
        $termIds = [];
        foreach ($values['taxonomy_term_tids'] as $taxonomy) {
          $termIds[] = $taxonomy['target_id'];
        }

        $taxonomy_terms['taxonomy_term_uuids'] = $this->taxonomyHelper->getUuidsFromTids($termIds);
      }
    }

    return $taxonomy_terms;
  }

  /**
   * {@inheritdoc}
   */
  public function accessCheck(array $values, FieldDefinitionInterface $field_definition): AccessResultInterface {
    $cache_contexts = [
      'session',
    ];

    $access_result = AccessResult::neutral();
    if (!empty($values['taxonomy_term_uuids'])) {
      $access_result = AccessResult::forbidden();

      $actual_taxonomy = $this->getActualTaxonomy($field_definition);
      // Transform tids to uuids so we can compare with EVP field storage.
      $actual_taxonomy = $this->taxonomyHelper->getUuidsFromTids($actual_taxonomy);
      foreach ($values['taxonomy_term_uuids'] as $uuid) {
        if (\in_array($uuid, $actual_taxonomy, TRUE)) {
          $access_result = AccessResult::neutral();
          break;
        }
      }
    }

    return $access_result->addCacheContexts($cache_contexts);
  }

  /**
   * Get the actual user taxonomy, handling normal browsing and preview.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition of the field where the access check is called.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The account to evaluate taxonomies for.
   *   Default to current user.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @return array
   *   The user taxonomy terms or the preview taxonomy terms.
   */
  public function getActualTaxonomy(FieldDefinitionInterface $field_definition, ?AccountInterface $account = NULL): array {
    if ($account === NULL) {
      $account = $this->currentUser;
    }
    $user_taxonomy_terms = [];
    // Anonymous.
    if ($account->isAnonymous()) {
      $state_key_elements = [
        'entity_visibility_preview',
        'anonymous',
        'taxonomy_condition',
        $field_definition->getTargetEntityTypeId(),
        $field_definition->getTargetBundle(),
        $field_definition->getName(),
      ];
      /** @var array $user_taxonomy_terms */
      $user_taxonomy_terms = $this->state->get(\implode(':', $state_key_elements), []);
    }
    else {
      $preview_taxonomy = $this->sessionManager->getFromSession('entity_visibility_preview_taxonomy');

      // Preview case.
      if ($preview_taxonomy !== NULL) {
        /** @var array $user_taxonomy_terms */
        $user_taxonomy_terms = $preview_taxonomy;
      }
      else {
        /** @var \Drupal\user\UserInterface $user */
        $user = $this->entityTypeManager->getStorage('user')->load($account->id());
        foreach ($this->configuration['user_fields'] as $user_field_name) {
          if ($user->hasField($user_field_name)) {
            /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $user_field */
            $user_field = $user->get($user_field_name);
            /** @var \Drupal\taxonomy\TermInterface[] $user_field_taxonomy_terms */
            $user_field_taxonomy_terms = $user_field->referencedEntities();
            foreach ($user_field_taxonomy_terms as $taxonomy_term) {
              $user_taxonomy_terms[] = $taxonomy_term->id();
            }
          }
        }
      }
    }

    return $user_taxonomy_terms;
  }

  /**
   * {@inheritdoc}
   */
  public function previewBuildForm(array &$form, FormStateInterface $form_state): array {
    $default_value = [];
    /** @var array|null $preview_taxonomy */
    $preview_taxonomy = $this->sessionManager->getFromSession('entity_visibility_preview_taxonomy');
    if ($preview_taxonomy !== NULL) {
      $default_value = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadMultiple($preview_taxonomy);
    }

    $bundles = $this->getPreviewBundles();
    $bundles = \array_combine($bundles, $bundles);

    switch ($this->taxonomyConditionConfig->get('taxonomy_selection_widget')) {
      case 'checkboxes':
        $form['preview_taxonomy'] = $this->getTaxonomyForm($bundles, (array) $preview_taxonomy);
        break;

      default:
        $form['preview_taxonomy'] = [
          '#type' => 'entity_autocomplete',
          '#title' => $this->t('User taxonomy terms'),
          '#description' => $this->t('Enter the taxonomy terms simulated for a user. The taxonomy terms of the user account used for the preview will not be used. Enter a comma-separated list.'),
          '#target_type' => 'taxonomy_term',
          '#selection_settings' => [
            'target_bundles' => $bundles,
          ],
          '#tags' => TRUE,
          '#default_value' => $default_value,
        ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function previewSubmitForm(array &$form, FormStateInterface $form_state): void {
    if ($this->taxonomyConditionConfig->get('taxonomy_selection_widget') == 'checkboxes') {
      $taxonomy_terms_uuids = $this->getUuidsFromCheckboxes($form_state->getValues());
      $taxonomy_terms = $this->taxonomyHelper->getTidsFromUuids($taxonomy_terms_uuids);
    }
    else {
      // Set the chosen display taxonomy terms in the session.
      $taxonomy_terms = [];
      /** @var array $preview_taxonomy */
      $preview_taxonomy = $form_state->getValue(['preview_taxonomy']);
      if (!empty($preview_taxonomy)) {
        foreach ($preview_taxonomy as $taxonomy) {
          $taxonomy_terms[] = $taxonomy['target_id'];
        }
      }
    }
    $this->sessionManager->setFromSession('entity_visibility_preview_taxonomy', $taxonomy_terms);
  }

  /**
   * {@inheritdoc}
   */
  public function previewResetForm(): void {
    $this->sessionManager->deleteFromSession('entity_visibility_preview_taxonomy');
  }

  /**
   * {@inheritdoc}
   */
  public function previewMessage(array &$items): void {
    /** @var array|null $preview_taxonomy_list */
    $preview_taxonomy_list = $this->sessionManager->getFromSession('entity_visibility_preview_taxonomy');
    if ($preview_taxonomy_list !== NULL && !empty($preview_taxonomy_list)) {
      $taxonomy_list = $this->entityTypeManager->getStorage('taxonomy_term')
        ->loadMultiple($preview_taxonomy_list);
      foreach ($taxonomy_list as $taxonomy) {
        $items[] = $this->t('User taxonomy: @taxonomy', [
          '@taxonomy' => $taxonomy->label(),
        ]);
      }
    }
  }

  /**
   * Get all the taxonomy term entity reference fields on user account.
   *
   * @return array
   *   The list of the taxonomy term entity reference fields on user account.
   */
  protected function getUserFieldsOptions(): array {
    $user_entity_reference_field = [];
    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $user_fields_definitions */
    $user_fields_definitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    foreach ($user_fields_definitions as $field_definition) {
      if ($field_definition->getType() == 'entity_reference') {
        if (
          isset($field_definition->getSettings()['handler'])
          && $field_definition->getSettings()['handler'] == 'default:taxonomy_term'
        ) {
          $user_entity_reference_field[$field_definition->getName()] = $field_definition->getLabel();
        }
      }
    }
    return $user_entity_reference_field;
  }

  /**
   * The taxonomy terms bundle list to use in checkboxes options.
   *
   * @return array
   *   The taxonomy terms bundle list.
   */
  protected function getBundlesOptions(): array {
    $bundle_options = [];
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('taxonomy_term');
    foreach ($bundles as $bundle_id => $bundle) {
      $bundle_options[$bundle_id] = $bundle['label'];
    }

    return $bundle_options;
  }

  /**
   * Returns list of bundles selected on configuration for the "preview" form.
   *
   * @return array
   *   An array of bundles configured for preview form.
   */
  protected function getPreviewBundles(): array {
    $bundles = (array) $this->taxonomyConditionConfig->get('preview_taxonomy_vocabularies');
    $bundles = \array_filter($bundles, static function ($value) {
      return $value != '0';
    });
    if (empty($bundles)) {
      $bundles = $this->getBundlesOptions();
    }
    return \array_keys($bundles);
  }

  /**
   * {@inheritdoc}
   */
  public function conditionDisplay($condition): array {
    $return = [];
    /** @var array $condition */
    if (!empty($condition['taxonomy_term_uuids'])) {
      $terms = $this->taxonomyHelper->getTermsFromUuids($condition['taxonomy_term_uuids']);
      if (!empty($terms)) {
        $return = [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'evp-display-taxonomy-container',
            ],
          ],
        ];
        $list = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#title' => $this->t('Accessible to users with any of those terms:'),
          '#items' => [],
        ];
        foreach ($terms as $term) {
          $vocabularyId = $term->bundle();
          $vocabulary = $this->taxonomyHelper->getVocabularyFromId($vocabularyId);
          if (!empty($vocabulary)) {
            $list['#items'][$vocabularyId]['#markup'] = $vocabulary->label();
          }
          $list['#items'][$vocabularyId]['children'][$term->id()]['#markup'] = $term->label() . ' (' . $term->id() . ')';
          \ksort($list['#items'][$vocabularyId]['children']);
        }
        \ksort($list['#items']);
        $return[] = $list;
      }
    }
    return $return;
  }

  /**
   * Returns taxonomy form as checkboxes (with hierarchy).
   *
   * @param array $bundles
   *   Vocabularies enabled.
   * @param array $tids
   *   Default values.
   *
   * @return array
   *   An array representing the taxonomy form.
   */
  protected function getTaxonomyForm(array $bundles, array $tids) {
    $form = [];
    try {
      foreach ($bundles as $vid) {
        $vocabulary = $this->taxonomyHelper->getVocabularyFromId($vid);
        if (empty($vocabulary)) {
          continue;
        }
        /** @var \Drupal\taxonomy\TermInterface[] $terms */
        $terms = $this->entityTypeManager
          ->getStorage('taxonomy_term')
          ->loadTree($vid, 0, NULL, TRUE);
        $form['taxonomy_tree_' . $vid] = [
          '#type' => 'checkboxes',
          '#title' => $vocabulary->label(),
          '#options' => [],
          '#attributes' => [
            'class' => [
              'evp-taxonomy-checkboxes-widget',
            ],
          ],
        ];
        foreach ($terms as $term) {
          /** @var string $term_uuid */
          $term_uuid = $term->uuid();
          // For each term, build a checkbox with a class to "indent" it.
          $form['taxonomy_tree_' . $vid][$term_uuid] = [
            '#type' => 'checkbox',
            '#title' => $term->label(),
            // @phpstan-ignore-next-line
            '#prefix' => "<div class='term-depth-" . $term->depth . "'>",
            '#suffix' => '</div>',
            '#return_value' => $term_uuid,
            '#default_value' => isset($tids[$term->id()]),
          ];
          // We have to add the option even if we don't "use" it (since it is
          // rendered 'manually' as a subitem) because Drupal displays an error
          // "illegal choice detected" if option is not present in #options.
          // @phpstan-ignore-next-line
          $form['taxonomy_tree_' . $vid]['#options'][$term_uuid] = $term_uuid;
        }
        $form['#attached']['library'] = 'entity_visibility_preview/field_widget';
      }
    }
    catch (\Exception $e) {
      // Do nothing, this shouldn't happen, it's just to clear the "throws"
      // out of the function.
    }
    return $form;
  }

  /**
   * Returns checked uuids for all enabled vocabularies.
   *
   * @param array $values
   *   Submitted values.
   *
   * @return array
   *   An array of selected term uuids.
   */
  protected function getUuidsFromCheckboxes(array $values) {
    $uuids = [];
    foreach ($values as $key => $field_value) {
      if (\strpos($key, 'taxonomy_tree_') === 0) {
        foreach ($field_value as $uuid => $value) {
          if (!empty($value)) {
            $uuids[] = $uuid;
          }
        }
      }
    }
    return $uuids;
  }

}
