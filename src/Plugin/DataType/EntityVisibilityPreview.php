<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\DataType;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\Attribute\DataType;
use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * The entity_visibility_preview data type.
 *
 * The plain value of a entity_visibility_preview is a serialized object
 * represented as a string.
 */
#[DataType(
  id: 'entity_visibility_preview',
  label: new TranslatableMarkup('Entity Visibility Preview'),
)]
class EntityVisibilityPreview extends StringData implements EntityVisibilityPreviewInterface {

}
