<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\DataType;

use Drupal\Core\TypedData\PrimitiveInterface;

/**
 * The entity_visibility_preview data type.
 *
 * The plain value of a entity_visibility_preview is a serialized object
 * represented as a string.
 */
interface EntityVisibilityPreviewInterface extends PrimitiveInterface {

}
