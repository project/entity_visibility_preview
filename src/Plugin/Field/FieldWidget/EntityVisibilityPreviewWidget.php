<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Attribute\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Advanced widget for entity_visibility_preview field.
 */
#[FieldWidget(
  id: 'entity_visibility_preview_widget',
  label: new TranslatableMarkup('Advanced Entity visibility preview form'),
  field_types: ['entity_visibility_preview'],
)]
class EntityVisibilityPreviewWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Instance of EntityVisibilityPreviewConditionPluginManager service.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityVisibilityPreviewConditionPluginManager = $container->get('plugin.manager.entity_visibility_preview_condition');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'open' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    $element['open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show widget details as opened by default'),
      '#description' => $this->t('If checked, the fieldset that wraps the Entity Visibility Preview field will initially be displayed expanded.'),
      '#default_value' => $this->getSetting('open'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->getSetting('open') ? $this->t('Opened by default.') : $this->t('Closed by default.');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\entity_visibility_preview\Plugin\Field\FieldType\EntityVisibilityPreviewItem $item */
    $item = $items[$delta];

    // Retrieve the values for each E.V.P.C. from the serialized array.
    $values = [];
    if (!empty($item->value)) {
      /** @var array $values */
      $values = \unserialize($item->value, [
        'allowed_classes' => FALSE,
      ]);
    }
    /** @var array $settings_enabled_condition */
    $settings_enabled_condition = $items->getSetting('enabled_condition_plugins');
    $plugins = $this->entityVisibilityPreviewConditionPluginManager->getEnabledDefinitions($settings_enabled_condition);
    /** @var array $condition_plugin_settings */
    $condition_plugin_settings = $items->getSetting('condition_plugins_settings');
    foreach ($plugins as $plugin_id => $plugin_definition) {
      /** @var array $configuration */
      $configuration = $condition_plugin_settings[$plugin_id] ?? [];
      /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entity_visibility_preview_condition */
      $entity_visibility_preview_condition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($plugin_id, $configuration);
      $plugin_values = $values[$plugin_id] ?? [];
      $entity_visibility_preview_condition_form = $entity_visibility_preview_condition->form($plugin_values);
      if (!empty($entity_visibility_preview_condition_form)) {
        $element[$plugin_id] = [
          '#type' => 'fieldset',
          '#title' => $plugin_definition['label']->render(),
        ];
        $element[$plugin_id][$plugin_id] = $entity_visibility_preview_condition_form;
      }
    }

    $element += [
      '#type' => 'details',
      '#open' => $this->getSetting('open'),
    ];
    // Put the form element into the form's "advanced" group if on a node.
    if ($item->getFieldDefinition()->getTargetEntityTypeId() == 'node') {
      $element += [
        '#group' => 'advanced',
      ];
    }

    // If the field has no plugin selected, hide the form.
    if (empty($plugins)) {
      $element = [];
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Flatten the values array to remove the groups and then serialize all the
    // visibility condition into one value for storage.
    foreach ($values as &$value) {
      $flattened_value = [];
      foreach ($value as $group) {
        if (\is_array($group)) {
          foreach ($group as $condition_id => $condition_value) {
            /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entityVisibilityPreviewCondition */
            $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($condition_id);
            $plugin_processed_values = $entityVisibilityPreviewCondition->formSubmit($condition_value);
            if (!empty($plugin_processed_values)) {
              $flattened_value[$condition_id] = $plugin_processed_values;
            }
          }
        }
      }
      $value = \serialize($flattened_value);
    }

    return $values;
  }

}
