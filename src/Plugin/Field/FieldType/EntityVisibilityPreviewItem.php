<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\Field\FieldType;

use Drupal\Core\Field\Attribute\FieldType;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\ComplexDataDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\TypedDataInterface;

/**
 * Plugin implementation of the 'entity_visibility_preview' field type.
 */
#[FieldType(
  id: 'entity_visibility_preview',
  label: new TranslatableMarkup('Entity Visibility Preview'),
  description: new TranslatableMarkup('This field stores conditional plugin.'),
  category: 'access',
  default_widget: 'entity_visibility_preview_widget',
  default_formatter: 'entity_visibility_preview_empty_formatter',
  cardinality: 1,
  serialized_property_names: [
    'value',
  ],
)]
class EntityVisibilityPreviewItem extends FieldItemBase {

  /**
   * The entity visibility preview condition plugin manager.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ComplexDataDefinitionInterface $definition, $name = NULL, ?TypedDataInterface $parent = NULL) {
    parent::__construct($definition, $name, $parent);
    $this->entityVisibilityPreviewConditionPluginManager = \Drupal::service('plugin.manager.entity_visibility_preview_condition');
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'enabled_condition_plugins' => [],
      'condition_plugins_settings' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['enabled_condition_plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled condition plugins'),
      '#options' => $this->getSettingOptions('enabled_condition_plugins'),
      '#default_value' => $this->getSetting('enabled_condition_plugins'),
    ];

    $element['condition_plugins_settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    /** @var array $condition_plugin_settings */
    $condition_plugin_settings = $this->getSetting('condition_plugins_settings');
    $plugin_definitions = $this->entityVisibilityPreviewConditionPluginManager->getDefinitions();
    /** @var array $plugin_definition */
    foreach ($plugin_definitions as $plugin_id => $plugin_definition) {
      /** @var array $configuration */
      $configuration = $condition_plugin_settings[$plugin_id] ?? [];

      /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $instance */
      $instance = $this->entityVisibilityPreviewConditionPluginManager->createInstance($plugin_id, $configuration);
      $instance_settings_form = $instance->fieldSettingsForm();

      if (!empty($instance_settings_form)) {
        $element['condition_plugins_settings'][$plugin_id] = [
          '#type' => 'details',
          '#title' => $this->t("@plugin_label plugin's settings", [
            '@plugin_label' => $plugin_definition['label']->render(),
          ]),
          '#states' => [
            'visible' => [
              ':input[name="settings[enabled_condition_plugins][' . $plugin_id . ']"]' => [
                'checked' => TRUE,
              ],
            ],
          ],
        ] + $instance_settings_form;
      }
    }

    return $element;
  }

  /**
   * Returns select options for a plugin setting.
   *
   * @param string $setting_name
   *   The name of the widget setting.
   *
   * @return array
   *   An array of setting option usable as a value for a "#options" key.
   */
  protected function getSettingOptions(string $setting_name): array {
    $options = [];
    switch ($setting_name) {
      case 'enabled_condition_plugins':
        $plugins = $this->entityVisibilityPreviewConditionPluginManager->getDefinitions();
        /** @var array $definition */
        foreach ($plugins as $plugin_id => $definition) {
          $options[$plugin_id] = $definition['label']->render();
        }
        break;
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['value'] = DataDefinition::create('entity_visibility_preview')
      ->setLabel(\t('entity_visibility_preview'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '' || $value === \serialize([]);
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(): void {
    parent::preSave();

    // Get the value about to be saved.
    // @phpstan-ignore-next-line
    $current_value = $this->value;
    // Only unserialize if still serialized string.
    if (\is_string($current_value)) {
      $current_conditions = \unserialize($current_value, ['allowed_classes' => FALSE]);
    }

    // Update the value to only save overridden tags.
    // @phpstan-ignore-next-line
    $this->value = \serialize($current_conditions);
  }

}
