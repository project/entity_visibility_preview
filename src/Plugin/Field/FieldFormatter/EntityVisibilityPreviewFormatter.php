<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A formatter that will display all conditions.
 */
#[FieldFormatter(
  id: 'entity_visibility_preview_formatter',
  label: new TranslatableMarkup('EVP conditions formatter'),
  field_types: [
    'entity_visibility_preview',
  ],
)]
class EntityVisibilityPreviewFormatter extends FormatterBase {

  /**
   * The preview condition plugin manager.
   *
   * @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionPluginManager
   */
  protected $entityVisibilityPreviewConditionPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityVisibilityPreviewConditionPluginManager = $container->get('plugin.manager.entity_visibility_preview_condition');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $view = [];
    if ($items->count() < 1) {
      return $view;
    }
    /** @var \Drupal\Core\TypedData\TypedDataInterface $itemsData */
    $itemsData = $items->get(0);
    /** @var array $values */
    $values = $itemsData->getValue();
    if (!empty($values['value'])) {
      /** @var array $entity_conditions */
      $entity_conditions = \unserialize($values['value'], ['allowed_classes' => FALSE]);
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
      $field_definition = $items->getFieldDefinition();
      /** @var array $enabled_condition_plugins */
      $enabled_condition_plugins = $field_definition->getSetting('enabled_condition_plugins');
      /** @var array $condition_plugins_settings */
      $condition_plugins_settings = $field_definition->getSetting('condition_plugins_settings');
      foreach ($entity_conditions as $condition_plugin_id => $condition_value) {
        // Check if the condition plugin is enabled in the field settings and
        // if we have configuration.
        if (
          \in_array($condition_plugin_id, $enabled_condition_plugins, TRUE)
          && !empty($enabled_condition_plugins[$condition_plugin_id])
          && $this->entityVisibilityPreviewConditionPluginManager->hasDefinition($condition_plugin_id)
        ) {
          $configuration = $condition_plugins_settings[$condition_plugin_id] ?? [];
          /** @var \Drupal\entity_visibility_preview\EntityVisibilityPreviewConditionInterface $entityVisibilityPreviewCondition */
          $entityVisibilityPreviewCondition = $this->entityVisibilityPreviewConditionPluginManager->createInstance($condition_plugin_id, $configuration);
          // We have to use the "0" key because if the returned array has more
          // than one key, field will display it as multiple values for the
          // field.
          $view[0][$condition_plugin_id] = $entityVisibilityPreviewCondition->conditionDisplay($condition_value);
        }
      }
    }

    if (!empty($view)) {
      $view['#attached']['library'][] = 'entity_visibility_preview/field_display';
    }
    return $view;
  }

}
