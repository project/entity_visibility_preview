<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * An empty formatter.
 */
#[FieldFormatter(
  id: 'entity_visibility_preview_empty_formatter',
  label: new TranslatableMarkup('Empty formatter'),
  field_types: [
    'entity_visibility_preview',
  ],
)]
class EntityVisibilityPreviewEmptyFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Does not actually output anything.
    return [];
  }

}
