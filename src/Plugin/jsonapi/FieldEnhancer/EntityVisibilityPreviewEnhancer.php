<?php

declare(strict_types=1);

namespace Drupal\entity_visibility_preview\Plugin\jsonapi\FieldEnhancer;

use Drupal\jsonapi_extras\Plugin\ResourceFieldEnhancerBase;
use Shaper\Util\Context;

/**
 * Prepare entity visibility preview value to be able to shared.
 *
 * @ResourceFieldEnhancer(
 *     id = "entity_visibility_preview",
 *     label = @Translation("Entity visibility preview (Entity visibility preview field only)"),
 *     description = @Translation("Prepare entity visibility preview value to be able to be shared.")
 * )
 */
class EntityVisibilityPreviewEnhancer extends ResourceFieldEnhancerBase {

  /**
   * {@inheritdoc}
   */
  protected function doUndoTransform($data, Context $context) {
    return [
      'value' => $data,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function doTransform($value, Context $context) {
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOutputJsonSchema() {
    return [
      'type' => 'object',
    ];
  }

}
